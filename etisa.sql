-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 09. Sep 2015 um 11:06
-- Server Version: 5.6.14
-- PHP-Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `etisa`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_assets`
--

CREATE TABLE IF NOT EXISTS `nobqg_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Daten für Tabelle `nobqg_assets`
--

INSERT INTO `nobqg_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 169, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 56, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.options":[],"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 57, 58, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 59, 60, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 61, 62, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 63, 64, 1, 'com_login', 'com_login', '{}'),
(13, 1, 65, 66, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 67, 68, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 69, 70, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 71, 72, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 73, 74, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 75, 136, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 137, 140, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 141, 142, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 143, 144, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 145, 146, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 147, 148, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 149, 152, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(26, 1, 153, 154, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 23, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 138, 139, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 150, 151, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 155, 156, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 157, 158, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 1, 159, 160, 1, 'com_tags', 'com_tags', '{"core.admin":[],"core.manage":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(36, 1, 161, 162, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 163, 164, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 165, 166, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 76, 77, 2, 'com_modules.module.1', 'Main Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[],"module.edit.frontend":[]}'),
(40, 18, 78, 79, 2, 'com_modules.module.2', 'Login', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 18, 80, 81, 2, 'com_modules.module.3', 'Popular Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 18, 82, 83, 2, 'com_modules.module.4', 'Recently Added Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(43, 18, 84, 85, 2, 'com_modules.module.8', 'Toolbar', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 18, 86, 87, 2, 'com_modules.module.9', 'Quick Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 18, 88, 89, 2, 'com_modules.module.10', 'Logged-in Users', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 18, 90, 91, 2, 'com_modules.module.12', 'Admin Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 18, 92, 93, 2, 'com_modules.module.13', 'Admin Submenu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 18, 94, 95, 2, 'com_modules.module.14', 'User Status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 18, 96, 97, 2, 'com_modules.module.15', 'Title', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 18, 98, 99, 2, 'com_modules.module.16', 'Login Form', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(51, 18, 100, 101, 2, 'com_modules.module.17', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(52, 18, 102, 103, 2, 'com_modules.module.79', 'Multilanguage status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(53, 18, 104, 105, 2, 'com_modules.module.86', 'Joomla Version', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(54, 18, 106, 107, 2, 'com_modules.module.87', 'JCK Manager', ''),
(55, 18, 108, 109, 2, 'com_modules.module.88', 'Dashboard', ''),
(56, 18, 110, 111, 2, 'com_modules.module.89', 'JCK Manager v6.4.4', ''),
(57, 18, 112, 113, 2, 'com_modules.module.90', 'COM_JCKMAN_CPANEL_SLIDER_MANAGER_LABEL', ''),
(58, 18, 114, 115, 2, 'com_modules.module.91', 'COM_JCKMAN_CPANEL_SLIDER_PLUGIN_LABEL', ''),
(59, 18, 116, 117, 2, 'com_modules.module.92', 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_CHECK_LABEL', ''),
(60, 18, 118, 119, 2, 'com_modules.module.93', 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_LAYOUT_MANAGER', ''),
(61, 18, 120, 121, 2, 'com_modules.module.94', 'COM_JCKMAN_CPANEL_SLIDER_BACKUP_LABEL', ''),
(62, 18, 122, 123, 2, 'com_modules.module.95', 'COM_JCKMAN_CPANEL_SLIDER_RESTORE_LABEL', ''),
(63, 18, 124, 125, 2, 'com_modules.module.96', 'COM_JCKMAN_CPANEL_SLIDER_SYNC_LABEL', ''),
(64, 1, 167, 168, 1, 'com_jckman', 'com_jckman', '{}'),
(65, 18, 126, 127, 2, 'com_modules.module.97', 'Kontaktmodul', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(66, 18, 128, 129, 2, 'com_modules.module.98', 'Philosophiemodul', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(67, 18, 130, 131, 2, 'com_modules.module.99', 'Partnermodul', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(68, 27, 19, 20, 3, 'com_content.article.1', 'Herzlich Willkommen', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(69, 8, 24, 49, 2, 'com_content.category.8', 'Themen', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(70, 8, 50, 55, 2, 'com_content.category.9', 'Andere Unterseiten', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(71, 18, 132, 133, 2, 'com_modules.module.100', 'Main Menu 2', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(72, 70, 51, 52, 3, 'com_content.article.2', 'Impressum & Datenschutz', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(73, 18, 134, 135, 2, 'com_modules.module.101', 'Themennavigationsmodul', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"module.edit.frontend":[]}'),
(75, 70, 53, 54, 3, 'com_content.article.4', 'Jobs', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(76, 69, 25, 26, 3, 'com_content.article.5', 'Sprechanlagen & Videoanlagen', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(77, 69, 27, 28, 3, 'com_content.article.6', 'Smarthome', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(78, 69, 29, 30, 3, 'com_content.article.7', 'Rolladensteuerung', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(79, 69, 31, 32, 3, 'com_content.article.8', 'Alarmanlagen', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(80, 69, 33, 34, 3, 'com_content.article.9', 'IP-Technik', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(81, 69, 35, 36, 3, 'com_content.article.10', 'Solarthermie', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(82, 69, 37, 38, 3, 'com_content.article.11', 'LED-Technik', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(83, 69, 39, 40, 3, 'com_content.article.12', 'Solartechnik & Photovoltaik', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(84, 69, 41, 42, 3, 'com_content.article.13', 'Elektroinstallation', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(85, 69, 43, 44, 3, 'com_content.article.14', 'Elektroservice', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(87, 69, 45, 46, 3, 'com_content.article.16', 'SPS Steuerungen', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(88, 69, 47, 48, 3, 'com_content.article.17', 'Step7 Programmierung', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(89, 27, 21, 22, 3, 'com_content.article.18', 'Unsere Inhalte sind noch in Bearbeitung', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_associations`
--

CREATE TABLE IF NOT EXISTS `nobqg_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_banners`
--

CREATE TABLE IF NOT EXISTS `nobqg_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_banner_clients`
--

CREATE TABLE IF NOT EXISTS `nobqg_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `nobqg_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_categories`
--

CREATE TABLE IF NOT EXISTS `nobqg_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `nobqg_categories`
--

INSERT INTO `nobqg_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 15, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 69, 1, 11, 12, 1, 'themen', 'com_content', 'Themen', 'themen', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 417, '2015-09-03 12:00:01', 0, '2015-09-03 12:00:01', 0, '*', 1),
(9, 70, 1, 13, 14, 1, 'andere-unterseiten', 'com_content', 'Andere Unterseiten', 'andere-unterseiten', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 417, '2015-09-03 12:00:08', 0, '2015-09-03 12:00:08', 0, '*', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_contact_details`
--

CREATE TABLE IF NOT EXISTS `nobqg_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_content`
--

CREATE TABLE IF NOT EXISTS `nobqg_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Daten für Tabelle `nobqg_content`
--

INSERT INTO `nobqg_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 68, 'Herzlich Willkommen', 'herzlich-willkommen', '<h1>\r\n	Herzlich Willkommen bei <b>ETISA</b><br />\r\n	<strong>Ihrem Elektro Technik Industrie Service aus Paderborn</strong></h1>\r\n<hr />\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	<img class="senol" src="images/etisa-geschaeftsfuehrer-senol-avci.png" /></p>\r\n', '', 1, 2, '2015-09-03 11:49:38', 417, '', '2015-09-03 11:50:50', 417, 0, '0000-00-00 00:00:00', '2015-09-03 11:49:38', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 1, '*', ''),
(2, 72, 'Impressum & Datenschutz', 'impressum', '<h1>\r\n	Impressum &amp; Datenschutz</h1>\r\n<p>\r\n	Senol Avci<br />\r\n	Stargarder Stra&szlig;e 2<br />\r\n	33098 Paderborn<br />\r\n	<br />\r\n	Telefon: 0 52 52 / 525252<br />\r\n	Mobil: 0152 / 533 97 434<br />\r\n	Fax: 0 52 52 / 525252<br />\r\n	Mail:<a href="mailto:info@etisa.de"> info@etisa.de</a></p>\r\n<br />\r\n<p>\r\n	Internet: www.etisa.de<br />\r\n	USt.IdNr.: DE55555555555</p>\r\n<p>\r\n	Haftungshinweis: Trotz inhaltlicher Kontrollen &uuml;bernehmen wir keine Haftung f&uuml;r die Inhalte anderer Internetpr&auml;senzen, die wir auf unserer Homepage verlinken. F&uuml;r diese externen Inhalte sind ausschlie&szlig;lich deren Betreiber verantwortlich.</p>\r\n<h3>\r\n	Nutzung und Weitergabe personenbezogener Daten</h3>\r\n<p>\r\n	Falls Sie uns personenbezogene Daten ausgeh&auml;ndigt haben, (z.B &uuml;ber unser Kontaktformular) verwenden wir diese ausschlie&szlig;lich zur Erf&uuml;llung Ihrer W&uuml;nsche und Anforderungen oder zur Beantwortung Ihrer Kontaktformularanfrage. Die Weitergabe, der Verkauf oder sonstige &Uuml;bermittlungen Ihrer Daten an Dritte erfolgt nicht.</p>\r\n<p>\r\n	Diese Internetpr&auml;senz nutzt sogenannte Cookies, kleine Textinformationenstr&auml;ger, die der Browser bei Bedarf auf Ihrem System abspeichert, wenn Sie unsere Internetseiten besuchen. Zur Analyse unserer Internetseite nutzen wir Piwik, eine Webanalyse-Software, die die Daten der Cookies auf unserem System speichert. Diese Daten werden nicht an Dritte weitergegeben.<br />\r\n	<br />\r\n	Ebenso verwendet Piwik &bdquo;Cookies&ldquo;, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung unserer Website erm&ouml;glichen. Die durch den Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website werden im Piwik gespeichert. Ihre IP-Adresse wird dabei so ver&auml;ndert und vom System verschleiert, dass eine Zuordnung zu Ihnen nicht mehr m&ouml;glich ist. Somit entsprechen wir also den Datenschutzbestimmungen.<br />\r\n	<br />\r\n	Sollten Sie nicht w&uuml;nschen, dass wir Ihren Computer wiedererkennen (Festplatten-Cookies), k&ouml;nnen Sie Ihren Browser so einstellen, dass er Cookies von Ihrer Computerfestplatte l&ouml;scht, alle Cookies blockiert oder Sie warnt, bevor ein Cookie gespeichert wird. Wir k&ouml;nnen bei einer solchen Einstellung allerdings nicht zusichern, dass unsere Funktionen ohne Einschr&auml;nkung nutzenbar sind.</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '', 1, 9, '2015-09-03 12:01:54', 417, '', '2015-09-07 15:48:57', 417, 417, '2015-09-07 15:48:57', '2015-09-03 12:01:54', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 15, 2, '', '', 1, 28, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 75, 'Jobs', 'jobs', '<h1>\r\n	Jobs</h1>\r\n', '', 1, 9, '2015-09-03 13:06:47', 417, '', '2015-09-03 13:06:47', 0, 0, '0000-00-00 00:00:00', '2015-09-03 13:06:47', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, '', '', 1, 3, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(5, 76, 'Sprechanlagen & Videoanlagen', 'sprechanlagen-videoanlagen', '<div class="heading">\r\n	<h1>\r\n		Sprechanlagen &amp; Videoanlagen</h1>\r\n	<h2>\r\n		Hier steht die Unter-Überschrift</h2>\r\n	<hr />\r\n</div>\r\n<div class="images clearfix">\r\n	<a class="fancybox" data-fancybox-group="gallery" href="images/1_b.jpg" title="Lorem ipsum dolor sit amet"><img alt="" src="images/1_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/2_b.jpg" title="Etiam quis mi eu elit temp"><img alt="" src="images/2_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/3_b.jpg" title="Cras neque mi, semper leon"><img alt="" src="images/3_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/4_b.jpg" title="Sed vel sapien vel sem uno"><img alt="" src="images/4_b.jpg" /></a></div>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n', '', 1, 8, '2015-09-03 13:52:34', 417, '', '2015-09-04 11:34:50', 417, 0, '0000-00-00 00:00:00', '2015-09-03 13:52:34', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 11, '', '', 1, 91, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(6, 77, 'Smarthome', 'smarthome', '<div class="heading">\r\n	<h1>\r\n		Smarthome</h1>\r\n	<h2>\r\n		Lorem Ipsum Dolor sit ahmet</h2>\r\n	<hr />\r\n</div>\r\n<div class="images clearfix">\r\n	<a class="fancybox" data-fancybox-group="gallery" href="images/1_b.jpg" title="Lorem ipsum dolor sit amet"><img alt="" src="images/1_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/2_b.jpg" title="Etiam quis mi eu elit temp"><img alt="" src="images/2_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/3_b.jpg" title="Cras neque mi, semper leon"><img alt="" src="images/3_b.jpg" /></a> <a class="fancybox" data-fancybox-group="gallery" href="images/4_b.jpg" title="Sed vel sapien vel sem uno"><img alt="" src="images/4_b.jpg" /></a></div>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n<h3>\r\n	Pastrami leberkas capicola sausage</h3>\r\n<p>\r\n	Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.</p>\r\n<p>\r\n	Short ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.</p>\r\n', '', 1, 8, '2015-09-03 15:40:19', 417, '', '2015-09-07 15:08:39', 417, 0, '0000-00-00 00:00:00', '2015-09-03 15:40:19', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 12, '', '', 1, 26, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(7, 78, 'Rolladensteuerung', 'rolladensteuerung', '<p>\r\n	Pups</p>\r\n', '', 1, 8, '2015-09-07 09:42:23', 417, '', '2015-09-07 09:42:23', 0, 0, '0000-00-00 00:00:00', '2015-09-07 09:42:23', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 10, '', '', 1, 4, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(8, 79, 'Alarmanlagen', 'alarmanlagen', '<p>\r\n	Dups</p>\r\n', '', 1, 8, '2015-09-07 10:01:32', 417, '', '2015-09-07 10:01:32', 0, 0, '0000-00-00 00:00:00', '2015-09-07 10:01:32', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 9, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(9, 80, 'IP-Technik', 'ip-technik', '<p>\r\n	Ul</p>\r\n', '', 1, 8, '2015-09-07 10:59:28', 417, '', '2015-09-07 10:59:28', 0, 0, '0000-00-00 00:00:00', '2015-09-07 10:59:28', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 8, '', '', 1, 13, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(10, 81, 'Solarthermie', 'solarthermie', '<p>\r\n	shdfjk</p>\r\n', '', 1, 8, '2015-09-07 11:01:01', 417, '', '2015-09-07 11:01:01', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:01:01', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 7, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(11, 82, 'LED-Technik', 'led-technik', '<p>\r\n	dfdsfsdfds</p>\r\n', '', 1, 8, '2015-09-07 11:04:00', 417, '', '2015-09-08 09:19:38', 417, 0, '0000-00-00 00:00:00', '2015-09-07 11:04:00', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":"\\/etisa.de\\/smarthome\\/ip-technik.html","urlatext":"IP-Technik","targeta":"0","urlb":"\\/etisa.de\\/solartechnik-und-photovoltaik.html","urlbtext":"Solartechnik & Photovoltaik","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 6, '', '', 1, 56, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(12, 83, 'Solartechnik & Photovoltaik', 'solartechnik-photovoltaik', '<p>\r\n	dsfsdfsdfsd</p>\r\n', '', 1, 8, '2015-09-07 11:05:01', 417, '', '2015-09-07 11:05:01', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:05:01', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 5, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(13, 84, 'Elektroinstallation', 'elektroinstallation', '<p>\r\n	dfgdfgsdf</p>\r\n', '', 1, 8, '2015-09-07 11:08:50', 417, '', '2015-09-07 11:08:50', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:08:50', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 4, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(14, 85, 'Elektroservice', 'elektroservice', '<p>\r\n	asdfasdfdasfs</p>\r\n', '', 1, 8, '2015-09-07 11:09:02', 417, '', '2015-09-07 11:09:02', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:09:02', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 3, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(16, 87, 'SPS Steuerungen', 'sps-steuerungen', '<p>\r\n	fyd cfgg</p>\r\n', '', 1, 8, '2015-09-07 11:09:32', 417, '', '2015-09-07 11:09:32', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:09:32', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(17, 88, 'Step7 Programmierung', 'step7-programmierung', '<p>\r\n	gfagshdjfjkhjh</p>\r\n', '', 1, 8, '2015-09-07 11:09:47', 417, '', '2015-09-07 11:09:47', 0, 0, '0000-00-00 00:00:00', '2015-09-07 11:09:47', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(18, 89, 'Unsere Inhalte sind noch in Bearbeitung', 'unsere-inhalte-sind-noch-in-bearbeitung', '<div class="heading">\r\n	<h1>\r\n		Wir bitten um Verständnis</h1>\r\n	<h2>\r\n		Unsere Inhalte sind zur Zeit noch in Bearbeitung</h2>\r\n	<hr />\r\n</div>\r\n<p>\r\n	Wir sind gerade dabei unsere Inhalte für Sie zu optimieren, vielen Dank für Ihr Verständnis. Ihr <strong>ETISA</strong> Team.</p>\r\n', '', 1, 2, '2015-09-07 15:08:27', 417, '', '2015-09-07 15:31:49', 417, 0, '0000-00-00 00:00:00', '2015-09-07 15:08:27', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, '', '', 1, 37, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_contentitem_tag_map`
--

CREATE TABLE IF NOT EXISTS `nobqg_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `nobqg_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_content_frontpage`
--

INSERT INTO `nobqg_content_frontpage` (`content_id`, `ordering`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_content_rating`
--

CREATE TABLE IF NOT EXISTS `nobqg_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_content_types`
--

CREATE TABLE IF NOT EXISTS `nobqg_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Daten für Tabelle `nobqg_content_types`
--

INSERT INTO `nobqg_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(4, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(6, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(9, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(11, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(12, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(13, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `nobqg_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_extensions`
--

CREATE TABLE IF NOT EXISTS `nobqg_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10009 ;

--
-- Daten für Tabelle `nobqg_extensions`
--

INSERT INTO `nobqg_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":"","filename":"mailto"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":"","filename":"wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":"","filename":"banners"}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '{"show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"de-DE","site":"de-DE"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":"","filename":"search"}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"article_layout":"_:default","show_title":"0","link_titles":"0","show_intro":"1","info_block_position":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_readmore":"1","show_readmore_title":"1","readmore_limit":"100","show_tags":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_heading_title_text":"1","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_featured":"show","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":"","filename":"users"}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"1","mail_to_admin":"0","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","show_tag_num_items":"0","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":"","filename":"contenthistory"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":"","filename":"ajax"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":"","filename":"simplepie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":"","filename":"phputf8"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"mediaversion":"d768ab9bac1696c139b0fa90abed7500"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":"","filename":"idna_convert"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2015-04-22 13:15:32","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2015 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.4.3","description":"LIB_FOF_XML_DESCRIPTION","group":"","filename":"fof"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"PHPass","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":"","filename":"phpass"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":"","filename":"mod_articles_archive"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_articles_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":"","filename":"mod_banners"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":"","filename":"mod_breadcrumbs"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":"","filename":"mod_footer"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_news"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":"","filename":"mod_random_image"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":"","filename":"mod_related_items"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":"","filename":"mod_search"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":"","filename":"mod_syndicate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":"","filename":"mod_users_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":"","filename":"mod_whosonline"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":"","filename":"mod_wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":"","filename":"mod_articles_category"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":"","filename":"mod_articles_categories"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":"","filename":"mod_languages"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":"","filename":"mod_finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":"","filename":"mod_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":"","filename":"mod_logged"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":"","filename":"mod_quickicon"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":"","filename":"mod_status"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":"","filename":"mod_submenu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":"","filename":"mod_title"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":"","filename":"mod_toolbar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":"","filename":"mod_multilangstatus"}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":"","filename":"mod_version"}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats_admin"}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_tags_popular"}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":"","filename":"mod_tags_similar"}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":"","filename":"gmail"}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":"","filename":"ldap"}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":"","filename":"emailcloak"}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":"","filename":"loadmodule"}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":"","filename":"pagenavigation"}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":"","filename":"vote"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others","authorEmail":"marijnh@gmail.com","authorUrl":"http:\\/\\/codemirror.net\\/","version":"5.3","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":"","filename":"codemirror"}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"September 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":"","filename":"none"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 0, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2014","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.1.7","description":"PLG_TINY_XML_DESCRIPTION","group":"","filename":"tinymce"}', '{"mode":"1","skin":"0","mobile":"0","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","html_height":"550","html_width":"750","resizing":"1","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","colors":"1","table":"1","smilies":"1","hr":"1","link":"1","media":"1","print":"1","directionality":"1","fullscreen":"1","alignment":"1","visualchars":"1","visualblocks":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":"","filename":"article"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":"","filename":"image"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":"","filename":"readmore"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0);
INSERT INTO `nobqg_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":"","filename":"languagefilter"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":"","filename":"p3p"}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":"","filename":"cache"}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":"","filename":"debug"}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":"","filename":"log"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION","group":"","filename":"redirect"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":"","filename":"remember"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":"","filename":"sef"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":"","filename":"logout"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":"","filename":"contactcreator"}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"autoregister":"1","mail_to_user":"1","forceLogout":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":"","filename":"profile"}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":"","filename":"languagecode"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":"","filename":"joomlaupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":"","filename":"extensionupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":"","filename":"recaptcha"}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":"","filename":"highlight"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":"","filename":"totp"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":"","filename":"cookie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":"","filename":"yubikey"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{"name":"plg_search_tags","type":"plugin","creationDate":"March 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"search_limit":"50","show_tagged_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.2","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.2","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(602, 'German (DE-CH-AT)', 'language', 'de-DE', '', 0, 1, 1, 0, '{"name":"German (Germany-Switzerland-Austria)","type":"language","creationDate":"03.07.2015","author":"J!German","copyright":"Copyright (C) 2005 - 2015 Open Source Matters & J!German. All rights reserved.","authorEmail":"team@jgerman.de","authorUrl":"http://www.jgerman.de","version":"3.4.3.1","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(603, 'German (DE-CH-AT)', 'language', 'de-DE', '', 1, 1, 1, 0, '{"name":"German (Germany-Switzerland-Austria)","type":"language","creationDate":"03.07.2015","author":"J!German","copyright":"Copyright (C) 2005 - 2015 Open Source Matters & J!German. All rights reserved.","authorEmail":"team@jgerman.de","authorUrl":"http://www.jgerman.de","version":"3.4.3.1","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(604, 'German Language Pack', 'package', 'pkg_de-DE', '', 0, 1, 1, 0, '{"name":"German Language Pack","type":"package","creationDate":"03.07.2015","author":"J!German","copyright":"","authorEmail":"team@jgerman.de","authorUrl":"http://www.jgerman.de","version":"3.4.3.1","group":"","filename":"pkg_de-DE"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"June 2015","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.3","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10000, 'etisa', 'template', 'etisa', '', 0, 1, 1, 0, '{"name":"etisa","type":"template","creationDate":"02. September 2015","author":"Senol Avci","copyright":"Copyright \\u00a9 etisa.de","authorEmail":"info@etisa.de","authorUrl":"http:\\/\\/www.etisa.de","version":"2.5.0","description":"Etisa Template","group":"","filename":"templateDetails"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `nobqg_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10001, 'Editor - JoomlaCK', 'plugin', 'jckeditor', 'editors', 0, 1, 1, 0, '{"name":"Editor - JoomlaCK","type":"plugin","creationDate":"Jan 2015","author":"WebxSolution Ltd","copyright":"","authorEmail":"","authorUrl":"","version":"6.6.2","description":"PLG_JCK_XML_DESC","group":"","filename":"jckeditor"}', '{"toolbar":"Publisher","toolbar_ft":"Basic","uicolor":"#D6E6F4","skin":"office2007","content_css":"1","content_css_custom":"","styles_css":"1","styles_css_custom":"","imagePath":"images","flashPath":"images\\/flash","filePath":"files","entermode":"2","shiftentermode":"2","wwidth":"","hheight":"","showerrors":"1","lang_mode":"1","lang_code":"en","bgcolor":"#FFFFFF","ftcolor":"","ftsize":"12","ftfamily":"Arial","textalign":"0","entities":"1","formatsource":"1","returnScript":"1","useUserFolders":"0","userFolderType":"username","displayFoldersTo":[],"filebrowsergroups":[],"forcesimpleAmpersand":"0","startupFocus":"0","EnableImageDragndrop":"1","imageDragndropPath":"images","jcktypography":"1","jcktypographycontent":"﻿\\/*\\r\\n * JCK Editor Typography Style-sheet\\r\\n * @author: Paul Franklin\\r\\n * @website: http:\\/\\/www.joomlackeditor.com\\r\\n * @version: 2.0\\r\\n * @copyright (C) WebxSolution Ltd 2011 - 2014. All rights reserved\\r\\n * @license: GPLv2\\r\\n * @terms: http:\\/\\/www.joomlackeditor.com\\/terms-of-use\\r\\n * @icons: WebxSolution Ltd has the non-exclusive, non-transferable, non-sublicensable right to use the Licensed Material an unlimited number of times in any and all media for the following commercial or personal purposes (together the \\"Permitted Uses\\") subject to the restrictions set forth in the Agreement. Any uses other than the Permitted Uses must be approved by DryIcons in writing. http:\\/\\/dryicons.com\\/terms\\/commercial\\/\\r\\n *\\/ \\r\\n\\r\\n \\/* ADD PARAGRAPH FORMAT STYLES FOR EDITOR\\r\\n-------------------------------------------------------------------------*\\/\\r\\n@font-face {}\\r\\nbody.cke_show_borders {}\\r\\nh1 {}\\r\\nh2 {}\\r\\nh3 {}\\r\\nh4 {}\\r\\nh5 {}\\r\\nh6 {}\\r\\npre {}\\r\\naddress {}\\r\\n\\r\\n \\/* Preformatted text\\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\nblockquote {\\r\\n    background: none repeat scroll 0 0 #CCCCCC;\\r\\n    border-left: 10px solid #8F8F8F;\\r\\n    color: #544C4A;\\r\\n    font: italic 12pt\\/1.2em Georgia;\\r\\n    margin: 10px !important;\\r\\n    padding: 10px !important;\\r\\n    text-shadow: 1px 1px 1px #ffffff;\\r\\n    width: 60%;\\r\\n    border-radius: 0 10px 0 10px;\\r\\n    -moz-border-radius: 0 10px 0 10px;\\r\\n    -webkit-border: 0 10px 0 10px;\\r\\n}\\r\\n\\r\\nblockquote p { display: inline; }\\r\\nblockquote:after { background: none repeat scroll 0 0 transparent !important; }\\r\\n\\r\\nblockquote:before {\\r\\n    content: url(\\"blockquotes.png\\") !important;\\r\\n\\tbackground: none repeat scroll 0 0 transparent !important;\\r\\n    margin-right: 15px;\\r\\n    vertical-align: super;\\r\\n\\tposition: relative !important;\\r\\n}\\r\\n\\r\\nspan.dropcap  {\\r\\n    color: #333333;\\r\\n    display: block;\\r\\n    float: left;\\r\\n    font: 60px\\/40px Georgia,Times,serif;\\r\\n    padding: 7px 8px 0 0;\\r\\n}\\r\\n\\r\\nspan.box-1heading-jck { font: 50px\\/50px Georgia,Times,serif;}\\r\\nspan.box-2heading-jck { font: 30px\\/40px Georgia,Times,serif;}\\r\\nspan.box-1heading-jck, span.box-2heading-jck { color: #333333; display: block; float: left;}\\r\\n\\r\\n\\/* Tooltips \\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\n.flashtip-jck, .ziptip-jck, .pdftip-jck, .videotip-jck, .infotip-jck, .warningtip-jck, .filmtip-jck, .pintip-jck , .lightbulbtip-jck , .recycletip-jck , .cameratip-jck, .commenttip-jck, .chattip-jck, .documenttip-jck, .accessibletip-jck, .startip-jck, .hearttip-jck, .previoustip-jck, .carttip-jck, .attachmenttip-jck, .calculatortip-jck, .cuttip-jck, .dollartip-jck, .poundtip-jck , .eurotip-jck, .mailtip-jck, .supporttip-jck, .nexttip-jck, .soundtip-jck { color: #444444 !important; }\\r\\n\\r\\na.flashtip-jck, a.ziptip-jck, a.pdftip-jck, a.videotip-jck, a.infotip-jck, a.warningtip-jck, a.filmtip-jck, a.pintip-jck , a.lightbulbtip-jck , a.recycletip-jck , a.cameratip-jck, a.commenttip-jck, a.chattip-jck, a.documenttip-jck, a.accessibletip-jck, a.startip-jck, a.hearttip-jck, a.previoustip-jck, a.carttip-jck, a.attachmenttip-jck, a.calculatortip-jck, a.cuttip-jck, a.dollartip-jck, a.poundtip-jck , a.eurotip-jck, a.mailtip-jck, a.supporttip-jck , a.nexttip-jck, a.soundtip-jck { color: #095197 !important; }\\r\\n\\r\\n.flashtip-jck, .ziptip-jck, .pdftip-jck, .videotip-jck, .infotip-jck, .warningtip-jck, .filmtip-jck, .pintip-jck , .lightbulbtip-jck , .recycletip-jck , .cameratip-jck, .commenttip-jck, .chattip-jck, .documenttip-jck, .accessibletip-jck, .startip-jck, .hearttip-jck, .previoustip-jck, .carttip-jck, .attachmenttip-jck, .calculatortip-jck, .cuttip-jck, .dollartip-jck, .poundtip-jck , .eurotip-jck, .mailtip-jck, .supporttip-jck, .nexttip-jck, .soundtip-jck, .download-DOC,\\r\\n a.flashtip-jck, a.ziptip-jck, a.pdftip-jck, a.videotip-jck, a.infotip-jck, a.warningtip-jck, a.filmtip-jck, a.pintip-jck , a.lightbulbtip-jck , a.recycletip-jck , a.cameratip-jck, a.commenttip-jck, a.chattip-jck, a.documenttip-jck, a.accessibletip-jck, a.startip-jck, a.hearttip-jck, a.previoustip-jck, a.carttip-jck, a.attachmenttip-jck, a.calculatortip-jck, a.cuttip-jck, a.dollartip-jck, a.poundtip-jck , a.eurotip-jck, a.mailtip-jck, a.supporttip-jck , a.nexttip-jck, a.soundtip-jck {\\r\\n    display: block;\\r\\n\\tmargin-bottom: 20px;\\r\\n\\tbackground-color: #FAFAFA !important;\\r\\n    background-position: left center !important;\\r\\n    background-repeat: no-repeat !important;\\r\\n\\tbackground-size: 28px auto !important;\\r\\n    border-bottom: 1px dotted #C8C8C8;\\r\\n    border-top: 1px dotted #C8C8C8;\\r\\n    display: block;\\r\\n    margin: 10px 0 !important;\\r\\n    padding: 7px 10px 7px 35px !important;\\r\\n\\t-webkit-transition: background-color 800ms ease-in-out;\\r\\n\\t-moz-transition: background-color 800ms ease-in-out;\\r\\n\\t-o-transition: background-color 800ms ease-in-out;\\r\\n\\ttransition: background-color 800ms ease-in-out;\\r\\n\\ttext-shadow: 1px 1px 1px #ffffff;\\r\\n\\tfont-size: 14px;\\r\\n}\\r\\n\\r\\na.flashtip-jck:hover, a.ziptip-jck:hover, a.pdftip-jck:hover, a.videotip-jck:hover, a.infotip-jck:hover, a.warningtip-jck:hover, a.filmtip-jck:hover, a.pintip-jck:hover , a.lightbulbtip-jck:hover , a.recycletip-jck:hover , a.cameratip-jck:hover, a.commenttip-jck:hover, a.chattip-jck:hover, a.documenttip-jck:hover, a.accessibletip-jck:hover, a.startip-jck:hover, a.hearttip-jck:hover, a.previoustip-jck:hover, a.carttip-jck:hover, a.attachmenttip-jck:hover, a.calculatortip-jck:hover, a.cuttip-jck:hover, a.dollartip-jck:hover, a.poundtip-jck:hover, a.eurotip-jck:hover, a.mailtip-jck:hover, a.supporttip-jck:hover, a.nexttip-jck:hover, a.soundtip-jck:hover, a.download-DOC:hover { color: #095197 !important; }\\r\\n\\r\\n.flashtip-jck:hover, .ziptip-jck:hover, .pdftip-jck:hover, .videotip-jck:hover, .infotip-jck:hover, .warningtip-jck:hover, .filmtip-jck:hover, .pintip-jck:hover , .lightbulbtip-jck:hover , .recycletip-jck:hover , .cameratip-jck:hover, .commenttip-jck:hover, .chattip-jck:hover, .documenttip-jck:hover, .accessibletip-jck:hover, .startip-jck:hover, .hearttip-jck:hover, .previoustip-jck:hover, .carttip-jck:hover, .attachmenttip-jck:hover, .calculatortip-jck:hover, .cuttip-jck:hover, .dollartip-jck:hover, .poundtip-jck:hover, .eurotip-jck:hover, .mailtip-jck:hover, .supporttip-jck:hover, .nexttip-jck:hover, .soundtip-jck:hover, .download-DOC:hover, a.flashtip-jck:hover, a.ziptip-jck:hover, a.pdftip-jck:hover, a.videotip-jck:hover, a.infotip-jck:hover, a.warningtip-jck:hover, a.filmtip-jck:hover, a.pintip-jck:hover , a.lightbulbtip-jck:hover, a.recycletip-jck:hover, a.cameratip-jck:hover, a.commenttip-jck:hover, a.chattip-jck:hover, a.documenttip-jck:hover, a.accessibletip-jck:hover, a.startip-jck:hover, a.hearttip-jck:hover, a.previoustip-jck:hover, a.carttip-jck:hover, a.attachmenttip-jck:hover, a.calculatortip-jck:hover, a.cuttip-jck:hover, a.dollartip-jck:hover, a.poundtip-jck:hover, a.eurotip-jck:hover, a.mailtip-jck:hover, a.supporttip-jck, a.nexttip-jck:hover, a.soundtip-jck:hover {\\r\\n    background-color: #F2F2F2 !important;\\r\\n\\t-webkit-transition: background-color 400ms ease-in-out;\\r\\n\\t-moz-transition: background-color 400ms ease-in-out;\\r\\n\\t-o-transition: background-color 400ms ease-in-out;\\r\\n\\ttransition: background-color 400ms ease-in-out;\\r\\n\\tbackground-repeat: no-repeat !important;\\r\\n}\\r\\n\\r\\n.flashtip-jck, .flashtip-jck:hover, a.flashtip-jck:hover { background-image: url(\\"flash.png\\")!important}\\r\\n.ziptip-jck, .ziptip-jck:hover, a.ziptip-jck:hover { background-image: url(\\"zip_download.png\\")!important}\\r\\n.pdftip-jck, .pdftip-jck:hover, a.pdftip-jck:hover { background-image: url(\\"pdf.png\\")!important}\\r\\n.videotip-jck , .videotip-jck:hover, a.videotip-jck:hover { background-image: url(\\"video_clip.png\\")!important}\\r\\n.infotip-jck, .infotip-jck:hover, a.infotip-jck:hover { background-image: url(\\"info.png\\")!important}\\r\\n.warningtip-jck, .warningtip-jck:hover, a.warningtip-jck:hover { background-image: url(\\"warning.png\\")!important}\\r\\n.filmtip-jck, .filmtip-jck:hover, a.filmtip-jck:hover { background-image: url(\\"film.png\\")!important}\\r\\n.pintip-jck, .pintip-jck:hover, a.pintip-jck:hover {background-image: url(\\"pin.png\\")!important}\\r\\n.lightbulbtip-jck, .lightbulbtip-jck:hover, a.lightbulbtip-jck:hover {background-image: url(\\"light_bulb.png\\")!important}\\r\\n.recycletip-jck, .recycletip-jck:hover, a.recycletip-jck:hover { background-image: url(\\"recycle.png\\")!important}\\r\\n.cameratip-jck, .cameratip-jck:hover, a.cameratip-jck:hover{ background-image: url(\\"camera.png\\")!important}\\r\\n.commenttip-jck, .commenttip-jck:hover, a.commenttip-jck:hover { background-image: url(\\"comment.png\\")!important}\\r\\n.chattip-jck, .chattip-jck:hover , a.chattip-jck:hover { background-image: url(\\"chat.png\\")!important}\\r\\n.documenttip-jck, .documenttip-jck:hover, a.documenttip-jck:hover { background-image: url(\\"document.png\\")!important}\\r\\n.accessibletip-jck, .accessibletip-jck:hover, a.accessibletip-jck:hover { background-image: url(\\"accessible.png\\")!important}\\r\\n.startip-jck, .startip-jck:hover , a.startip-jck:hover { background-image: url(\\"star.png\\")!important}\\r\\n.hearttip-jck, .hearttip-jck:hover, a.hearttip-jck:hover { background-image: url(\\"heart.png\\")!important}\\r\\n.previoustip-jck, .previoustip-jck:hover, a.previoustip-jck:hover { background-image: url(\\"previous.png\\")!important}\\r\\n.carttip-jck, .carttip-jck :hover, a.carttip-jck:hover { background-image: url(\\"cart.png\\")!important}\\r\\n.attachmenttip-jck, .attachmenttip-jck:hover, a.attachmenttip-jck:hover { background-image: url(\\"attachment.png\\")!important}\\r\\n.calculatortip-jck, .calculatortip-jck:hover , a.calculatortip-jck:hover { background-image: url(\\"calculator.png\\")!important}\\r\\n.cuttip-jck, .cuttip-jck:hover, a.cuttip-jck:hover { background-image: url(\\"cut.png\\")!important}\\r\\n.dollartip-jck , .dollartip-jck:hover, a.dollartip-jck:hover { background-image: url(\\"dollar_currency_sign.png\\")!important}\\r\\n.poundtip-jck, .poundtip-jck:hover, a.poundtip-jck:hover { background-image: url(\\"sterling_pound_currency_sign.png\\")!important}\\r\\n.eurotip-jck, .eurotip-jck:hover , a.eurotip-jck:hover { background-image: url(\\"euro_currency_sign.png\\")!important}\\r\\n.mailtip-jck, .mailtip-jck:hover , a.mailtip-jck:hover { background-image: url(\\"mail.png\\")!important}\\r\\n.supporttip-jck, .supporttip-jck:hover , a.supporttip-jck:hover { background-image: url(\\"support.png\\")!important}\\r\\n.nexttip-jck, .nexttip-jck:hover, a.nexttip-jck:hover { background-image: url(\\"next.png\\")!important}\\r\\n.soundtip-jck, .soundtip-jck:hover , a.soundtip-jck:hover { background-image: url(\\"sound.png\\")!important}\\r\\n\\r\\n\\r\\n\\r\\n \\/* Text Highlight\\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\nspan.blue_hlight-jck, span.gree_hlight-jck, span.red_hlight-jck, span.black_hlight-jck , span.yell_hlight-jck {\\r\\n    border-radius: 5px 5px 5px 5px;\\r\\n    color: #FFFFFF;\\r\\n    display: inline;\\r\\n    font-weight: bold;\\r\\n\\ttext-shadow: none;\\r\\n    padding: 2px 4px;\\r\\n\\tfont-size: 13px;\\r\\n}\\r\\n\\r\\nspan.blue_hlight-jck {  background: none repeat scroll 0 0 #3E6A86;}\\r\\nspan.gree_hlight-jck {  background: none repeat scroll 0 0 #b9cd96;}\\r\\nspan.red_hlight-jck {  background: none repeat scroll 0 0 #AA1428;}\\r\\nspan.black_hlight-jck {  background: none repeat scroll 0 0 #000000;}\\r\\nspan.yell_hlight-jck {  background: none repeat scroll 0 0 #F2F096; color: #544C4A;}\\r\\n \\r\\n\\/* Box Styles\\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\n.blubox-jck, .grebox-jck, .redbox-jck, .blabox-jck, .yelbox-jck   {\\r\\n\\tbackground: none repeat scroll 0 0 #FAFAFA;\\r\\n\\tborder-style: solid;\\r\\n    border-width: 1px 1px 1px 8px;\\r\\n\\tmargin: 10px 0 20px !important;\\r\\n    padding: 8px 8px 8px 20px !important;\\r\\n\\t-webkit-border-top-left-radius: 15px;\\r\\n    -webkit-border-bottom-left-radius: 15px;\\r\\n    -moz-border-radius-topleft: 15px;\\r\\n    -moz-border-radius-bottomleft: 15px;\\r\\n    border-top-left-radius: 15px;\\r\\n    border-bottom-left-radius: 15px;\\r\\n\\tcolor: #444444 !important;\\r\\n\\ttext-shadow: 1px 1px 1px #ffffff;\\r\\n}\\r\\n\\r\\n.blubox-jck  {  border-color: #DDDDDD #DDDDDD #DDDDDD #3E6A86;}\\r\\n.grebox-jck  {  border-color: #DDDDDD #DDDDDD #DDDDDD #b9cd96;}\\r\\n.redbox-jck {  border-color: #DDDDDD #DDDDDD #DDDDDD #AA1428;}\\r\\n.blabox-jck  {  border-color: #DDDDDD #DDDDDD #DDDDDD #000000;}\\r\\n.yelbox-jck  {  border-color: #DDDDDD #DDDDDD #DDDDDD #F2F096; color: #544C4A;}\\r\\n\\r\\n.blubox-jck > br,\\r\\n.grebox-jck  > br,\\r\\n.redbox-jck > br, \\r\\n.blabox-jck  > br, \\r\\n.yelbox-jck > br {\\r\\n    clear: both;\\r\\n}\\r\\n\\r\\n\\/* Icon Library\\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\n.info-jck, .warning-jck, .film-jck, .pin-jck , .lightbulb-jck , .recycle-jck , .camera-jck, .comment-jck, .chat-jck, .document-jck, .accessible-jck, .star-jck, .heart-jck, .previous-jck, .cart-jck, .attachment-jck, .calculator-jck, .cut-jck, .dollar-jck, .pound-jck , .euro-jck, .mail-jck, .support-jck, .next-jck, .sound-jck, .flash-jck, .zip-jck, .pdf-jck, .video-jck   {\\r\\n    display: block;\\r\\n    padding: 20px 0 20px 60px !important;\\r\\n\\tmargin-bottom: 20px !important;\\r\\n\\tbackground-color: transparent !important;\\r\\n    background-position: left center !important;\\r\\n    background-repeat: no-repeat !important;\\r\\n}\\r\\n\\r\\n.info-jck { background: url(\\"info.png\\")}\\r\\n.warning-jck { background: url(\\"warning.png\\")}\\r\\n.film-jck { background: url(\\"film.png\\")}\\r\\n.pin-jck {background: url(\\"pin.png\\")}\\r\\n.lightbulb-jck {background: url(\\"light_bulb.png\\")}\\r\\n.recycle-jck { background: url(\\"recycle.png\\")}\\r\\n.camera-jck { background: url(\\"camera.png\\")}\\r\\n.comment-jck { background: url(\\"comment.png\\")}\\r\\n.chat-jck  { background: url(\\"chat.png\\")}\\r\\n.document-jck { background: url(\\"document.png\\")}\\r\\n.accessible-jck { background: url(\\"accessible.png\\")}\\r\\n.star-jck { background: url(\\"star.png\\")}\\r\\n.heart-jck { background: url(\\"heart.png\\")}\\r\\n.previous-jck { background: url(\\"previous.png\\")}\\r\\n.cart-jck { background: url(\\"cart.png\\")}\\r\\n.attachment-jck { background: url(\\"attachment.png\\")}\\r\\n.calculator-jck { background: url(\\"calculator.png\\")}\\r\\n.cut-jck { background: url(\\"cut.png\\")}\\r\\n.dollar-jck { background: url(\\"dollar_currency_sign.png\\")}\\r\\n.pound-jck { background: url(\\"sterling_pound_currency_sign.png\\")}\\r\\n.euro-jck { background: url(\\"euro_currency_sign.png\\")}\\r\\n.mail-jck { background: url(\\"mail.png\\")}\\r\\n.support-jck { background: url(\\"support.png\\")}\\r\\n.next-jck  { background: url(\\"next.png\\")}\\r\\n.sound-jck  { background: url(\\"sound.png\\")}\\r\\n.flash-jck  { background: url(\\"flash.png\\")}\\r\\n.zip-jck  { background: url(\\"zip_download.png\\")}\\r\\n.pdf-jck  { background: url(\\"pdf.png\\")}\\r\\n.video-jck  { background: url(\\"video_clip.png\\")}\\r\\n\\r\\n\\/* Images Caption Styles\\r\\n-------------------------------------------------------------------------*\\/\\r\\nimg.caption { background-color:inherit; vertical-align: middle;}\\r\\n.img_caption.none { margin: 0 !important;} \\r\\n.img_caption { text-align: center; }\\r\\n\\r\\nimg.caption.photo, img.caption.photoblack, img.caption.photoblue, img.caption.photogreen, img.caption.photored, img.caption.photoyellow { \\r\\n    background-color: #FFFFFF;\\r\\n    padding: 10px 10px 40px;\\r\\n\\tmargin-right: 5px;\\r\\n\\tbox-shadow: 0 0 3px #000000;\\r\\n\\t-webkit-box-shadow: 0 0 3px #000000;\\r\\n    -moz-box-shadow: 0 0 3px #000000;\\r\\n}\\r\\n\\r\\nimg.caption.photo + p, img.caption.photoblack + p, img.caption.photoblue + p, img.caption.photogreen + p, img.caption.photored + p, img.caption.photoyellow + p {\\r\\n    position: relative; \\r\\n    left: 0px;\\r\\n    bottom: 30px;\\r\\n    overflow: hidden;\\r\\n    text-overflow: ellipsis;\\r\\n    white-space: nowrap;\\r\\n    width: 100%;\\r\\n\\tcolor: #333333;\\r\\n\\tmargin: 0;\\r\\n    font-size: 13px;\\r\\n    line-height: 18px;\\r\\n}\\r\\n\\r\\nimg.caption.photoblack + p , img.caption.photoblue + p, img.caption.photored + p { color: #FFFFFF; }\\r\\nimg.caption.photoblack { background-color: #000000;}\\r\\nimg.caption.photoblue {  background-color: #3E6A86;}\\r\\nimg.caption.photogreen  {  background-color: #b9cd96;}\\r\\nimg.caption.photored {  background-color: #AA1428;}\\r\\nimg.caption.photoyellow {  background-color: #F2F096;}\\r\\n\\r\\n\\/* Images Styles\\r\\n-------------------------------------------------------------------------*\\/\\r\\nimg.jck_img_align_left {\\r\\n    float: left; \\r\\n    margin: 3px 5px 0 0;\\r\\n    padding: 1px;\\r\\n}\\r\\n\\r\\nimg.jck_img_align_right {\\r\\n    float: right; \\r\\n    margin: 3px 0 0 5px;\\r\\n    padding: 1px;\\r\\n}\\r\\n\\r\\nimg.image_holder {\\r\\n    background: none repeat scroll 0 0 #FFFFFF;\\r\\n    border: 5px solid #EFEFEF;\\r\\n    margin: 3px 5px 0 0;\\r\\n    padding: 1px;\\r\\n}\\r\\n\\r\\nimg.fade_in {\\r\\n\\t-webkit-transition: all 500ms ease-in-out;\\r\\n\\t-moz-transition: all 500ms ease-in-out;\\r\\n\\t-o-transition: all 500ms ease-in-out;\\r\\n\\ttransition: all 500ms ease-in-out;\\r\\n\\t-webkit-box-shadow: 0 0 3px #000000;\\r\\n\\t-moz-box-shadow: 0 0 3px #000000;\\r\\n\\tbox-shadow: 0 0 3px #000000;\\r\\n\\tborder: 10px solid #FFFFFF;\\r\\n    opacity: 0.5;\\r\\n    overflow: hidden;\\r\\n    position: relative;\\r\\n\\tmargin: 3px;\\r\\n\\tcursor:url(cursor_zoom.png),auto;\\r\\n}\\r\\n\\r\\nimg.fade_in:hover {\\r\\n\\t-webkit-box-shadow: 0 0 10px #000000;\\r\\n\\t-moz-box-shadow: 0 0 10px #000000;\\r\\n\\tbox-shadow: 0 0 10px #000000;\\r\\n    opacity: 1;\\r\\n}\\r\\n\\r\\nimg.zoom  { \\r\\n\\t-webkit-transition: all 500ms ease-in-out;\\r\\n\\t-moz-transition: all 500ms ease-in-out;\\r\\n\\t-o-transition: all 500ms ease-in-out;\\r\\n\\ttransition: all 500ms ease-in-out;\\r\\n    background: none repeat scroll 0 0 #FFFFFF;\\r\\n\\t-webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n\\t-moz-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n\\tbox-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n    color: #333333;\\r\\n    display: inline;\\r\\n    float: left;\\r\\n    font-size: 18px;\\r\\n    padding: 10px 10px 15px;\\r\\n    text-align: center;\\r\\n    text-decoration: none;\\r\\n    width: auto;\\r\\n\\tfont-family: serif;\\r\\n\\theight: 100px;\\r\\n\\twidth: auto;\\r\\n\\tmargin: 0 20px 27px 0px;\\r\\n\\tcursor:url(cursor_zoom.png),auto;\\r\\n}\\r\\n\\r\\nimg.zoom:hover {\\r\\n    -moz-transform: scale(1.75);\\r\\n\\t-webkit-transform: scale(1.75);\\r\\n\\t-o-transform: scale(1.75);\\r\\n\\t-webkit-transition: all 500ms ease-in-out;\\r\\n\\t-moz-transition: all 500ms ease-in-out;\\r\\n\\t-o-transition: all 500ms ease-in-out;\\r\\n\\ttransition: all 500ms ease-in-out;\\r\\n\\t-webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);\\r\\n\\t-moz-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);\\r\\n\\tbox-shadow: 0 3px 6px rgba(0, 0, 0, 0.5); \\r\\n    position: relative;\\r\\n    z-index: 50;\\r\\n\\tmargin: 0 -60px 27px 80px;\\r\\n}\\r\\n\\r\\nimg.polaroids_zoom  {\\r\\n    -moz-transform: rotate(-2deg);\\r\\n\\t-webkit-transform: rotate(2deg); \\r\\n\\t-o-transform: rotate(2deg); \\r\\n\\t-webkit-transition: all 500ms ease-in-out;\\r\\n\\t-moz-transition: all 500ms ease-in-out;\\r\\n\\t-o-transition: all 500ms ease-in-out;\\r\\n\\ttransition: all 500ms ease-in-out;\\r\\n    background: none repeat scroll 0 0 #FFFFFF;\\r\\n\\t-webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n\\t-moz-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n\\tbox-shadow: 0 3px 6px rgba(0, 0, 0, 0.25);\\r\\n    color: #333333;\\r\\n    display: inline;\\r\\n    float: left;\\r\\n    font-size: 18px;\\r\\n    margin: 10px 20px 25px 14px;\\r\\n    padding: 10px 10px 15px;\\r\\n    text-align: center;\\r\\n    text-decoration: none;\\r\\n    width: auto;\\r\\n\\tfont-family: serif;\\r\\n\\theight: 100px;\\r\\n\\twidth: auto;\\r\\n\\tcursor:url(cursor_zoom.png),auto;\\r\\n}\\r\\n\\r\\nimg.polaroids_zoom:hover {\\r\\n    -moz-transform: scale(1.30);\\r\\n\\t-webkit-transform: scale(1.30);\\r\\n\\t-o-transform: scale(1.30);\\r\\n\\t-webkit-transition: all 500ms ease-in-out;\\r\\n\\t-moz-transition: all 500ms ease-in-out;\\r\\n\\t-o-transition: all 500ms ease-in-out;\\r\\n\\ttransition: all 500ms ease-in-out;\\r\\n\\t-webkit-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);\\r\\n\\t-moz-box-shadow: 0 3px 6px rgba(0, 0, 0, 0.5);\\r\\n\\tbox-shadow: 0 3px 6px rgba(0, 0, 0, 0.5); \\r\\n    position: relative;\\r\\n    z-index: 50;\\r\\n}\\r\\n\\r\\nimg.screenshot_blue, img.screenshot_green , img.screenshot_red, img.screenshot_black, img.screenshot_gray, img.screenshot_yellow  {\\r\\n    margin: 5px 0;\\r\\n    padding: 10px;\\r\\n\\tbackground: #ffffff;\\r\\n\\tbackground: -moz-linear-gradient(top, #ffffff 0%, #e5e5e5 100%);\\r\\n\\tbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5));\\r\\n\\tbackground: -webkit-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: -o-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: -ms-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#ffffff'', endColorstr=''#e5e5e5'',GradientType=0 );\\r\\n\\topacity: 1;\\r\\n\\tmax-width: 85%;\\r\\n}\\r\\n\\r\\nimg.screenshot_blue { border: 10px solid #3E6A86;}\\r\\nimg.screenshot_green  { border: 10px solid #b9cd96;}\\r\\nimg.screenshot_red  { border: 10px solid #AA1428;}\\r\\nimg.screenshot_black  { border: 10px solid #000000;}\\r\\nimg.screenshot_gray { border: 10px solid #F0F0F0;}\\r\\nimg.screenshot_yellow { border: 10px solid #EFDE2C;}\\r\\n\\r\\n \\/* Div Styles\\r\\n----------------------------------------------------------------------------------------------------*\\/\\r\\ndiv.scroll_box{\\r\\n    margin: 5px 0;\\r\\n    padding: 10px;\\r\\n\\tbackground: #ffffff;\\r\\n\\tbackground: -moz-linear-gradient(top, #ffffff 0%, #e5e5e5 100%);\\r\\n\\tbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5));\\r\\n\\tbackground: -webkit-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: -o-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: -ms-linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tbackground: linear-gradient(top, #ffffff 0%,#e5e5e5 100%);\\r\\n\\tfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#ffffff'', endColorstr=''#e5e5e5'',GradientType=0 );\\r\\n\\topacity: 1;\\r\\n\\tborder-left: 15px solid ##EDEDED;\\r\\n    border-top: 15px solid #EDEDED;\\r\\n    height: 300px;\\r\\n    overflow: scroll;\\r\\n\\t-webkit-box-shadow: 0 0 1px 1px #E3E3E3 inset, 0 0 1px 2px #FFFFFF inset, 0 0 0 1px #E3E3E3;\\r\\n\\tbox-shadow: 0 0 1px 1px #E3E3E3 inset, 0 0 1px 2px #FFFFFF inset, 0 0 0 1px #E3E3E3; \\r\\n}\\r\\n\\r\\ndiv.img_rollover { background-color: transparent; \\/* max-width: 0;  = set the the width of your rollover images*\\/ }\\r\\ndiv.img_rollover img:first-child { display: block; }\\r\\ndiv.img_rollover img:last-child { display: none; }\\r\\ndiv.img_rollover:hover img:first-child { display: none; }\\r\\ndiv.img_rollover:hover img:last-child { display:block; cursor: pointer; }\\r\\n\\r\\n\\/* Tables Styles\\r\\n-------------------------------------------------------------------------*\\/\\r\\ntable.table_style_blue, table.table_style_green, table.table_style_red, table.table_style_black, table.table_style_yellow  {\\r\\n    border: 1px solid #DDDDDD;\\r\\n    border-collapse: collapse;\\r\\n    color: #404040;\\r\\n    width: 100%;\\r\\n}\\r\\n\\r\\ntable.table_style_blue tbody tr, table.table_style_green tbody tr, table.table_style_red tbody tr, table.table_style_black tbody tr, table.table_style_yellow tbody tr  {\\r\\n    background: none repeat scroll 0 0 #F2F2F2;\\r\\n\\tborder: 1px solid #DDDDDD;\\r\\n\\t-webkit-transition: all 800ms ease-in-out;\\r\\n\\t-moz-transition: all 800ms ease-in-out;\\r\\n\\t-o-transition: all 800ms ease-in-out;\\r\\n\\ttransition: all 800ms ease-in-out;\\r\\n}\\r\\n\\r\\ntable.table_style_blue tbody tr:hover , table.table_style_green tbody tr:hover, table.table_style_red tbody tr:hover, table.table_style_black tbody tr:hover, table.table_style_yellow tbody tr:hover  {\\r\\n    background: none repeat scroll 0 0 #E5E5E5;\\r\\n\\t-webkit-transition: all 300ms ease-in-out;\\r\\n\\t-moz-transition: all 300ms ease-in-out;\\r\\n\\t-o-transition: all 300ms ease-in-out;\\r\\n\\ttransition: all 300ms ease-in-out;\\r\\n}\\r\\n\\r\\ntable.table_style_blue tbody tr td, table.table_style_green tbody tr td, table.table_style_red tbody tr td, table.table_style_black tbody tr td, table.table_style_yellow tbody tr td {\\r\\n    line-height: 22px;\\r\\n\\tpadding: 5px;\\r\\n\\tborder: 1px solid #DDDDDD;\\r\\n}\\r\\n\\r\\ntable.table_style_blue caption, table.table_style_green caption, table.table_style_red caption, table.table_style_black caption, table.table_style_yellow caption  {\\r\\n    color: #FFFFFF;\\r\\n    font-weight: 700;\\r\\n    line-height: 22px;\\r\\n    text-align: center;\\r\\n    text-transform: uppercase;\\r\\n}\\r\\n\\r\\ntable.table_style_blue caption { background: none repeat scroll 0 0 #3E6A86;}\\r\\ntable.table_style_green caption { background: none repeat scroll 0 0 #b9cd96;}\\r\\ntable.table_style_red caption { background: none repeat scroll 0 0 #AA1428;}\\r\\ntable.table_style_black caption { background: none repeat scroll 0 0 #000000;}\\r\\ntable.table_style_yellow caption { background: none repeat scroll 0 0 #F2F096; color: #544C4A;}\\r\\n\\r\\n\\/* Templates\\r\\n-------------------------------------------------------------------------*\\/\\r\\n.row-fluid { width: 100%; }\\r\\n.row-fluid:after { clear: both; }\\r\\n.row-fluid [class*=\\"span\\"]:first-child {  margin-left: 0; }\\r\\n.row-fluid .controls-row [class*=\\"span\\"] + [class*=\\"span\\"] { margin-left: 2.12766%; }\\r\\n.row-fluid .span6 { width: 48.9362%; }\\r\\n.row-fluid:before, .row-fluid:after {\\r\\n    content: \\"\\";\\r\\n    display: table;\\r\\n    line-height: 0;\\r\\n}\\r\\n.row-fluid [class*=\\"span\\"] {\\r\\n    -moz-box-sizing: border-box;\\r\\n    display: block;\\r\\n    float: left;\\r\\n    margin-left: 2.12766%;\\r\\n    min-height: 30px;\\r\\n    width: 100%;\\r\\n}\\r\\n\\r\\n\\/* 1 big button template *\\/\\r\\na.button-big {\\r\\n\\t-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;\\r\\n\\t-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;\\r\\n\\tbox-shadow:inset 0px 1px 0px 0px #ffffff;\\r\\n\\tbackground:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );\\r\\n\\tbackground:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );\\r\\n\\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr=''#ededed'', endColorstr=''#dfdfdf'');\\r\\n\\tbackground-color:#ededed;\\r\\n\\t-webkit-border-top-left-radius:6px;\\r\\n\\t-moz-border-radius-topleft:6px;\\r\\n\\tborder-top-left-radius:6px;\\r\\n\\t-webkit-border-top-right-radius:6px;\\r\\n\\t-moz-border-radius-topright:6px;\\r\\n\\tborder-top-right-radius:6px;\\r\\n\\t-webkit-border-bottom-right-radius:6px;\\r\\n\\t-moz-border-radius-bottomright:6px;\\r\\n\\tborder-bottom-right-radius:6px;\\r\\n\\t-webkit-border-bottom-left-radius:6px;\\r\\n\\t-moz-border-radius-bottomleft:6px;\\r\\n\\tborder-bottom-left-radius:6px;\\r\\n\\ttext-indent:0;\\r\\n\\tborder:1px solid #dcdcdc;\\r\\n\\tdisplay:inline-block;\\r\\n\\tcolor:#777777;\\r\\n\\tfont-family:sans-serif;\\r\\n\\tfont-size:18px;\\r\\n\\tfont-weight:bold;\\r\\n\\tfont-style:normal;\\r\\n\\tpadding: 10% 15%;\\r\\n\\ttext-decoration:none;\\r\\n\\ttext-align:center;\\r\\n\\ttext-shadow:1px 1px 0px #ffffff;\\r\\n\\ttext-transform: uppercase;\\r\\n\\tmargin: 10px 0;\\r\\n}\\r\\na.button-big:hover {\\r\\n\\tbackground:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );\\r\\n\\tbackground:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );\\r\\n\\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr=''#dfdfdf'', endColorstr=''#ededed'');\\r\\n\\tbackground-color:#dfdfdf;\\r\\n}\\r\\na.button-big:active {\\r\\n\\tposition:relative;\\r\\n\\ttop:1px;\\r\\n}\\r\\n\\/* 2 big button with desc *\\/\\r\\n.row-fluid .span6 {  width: 48.9362%; }\\r\\n\\/* 3 big button with desc *\\/\\r\\n.row-fluid .span4 { width: 31.9149%; }\\r\\n\\r\\n\\/* User Profiles *\\/\\r\\ndiv.row-fluid img.polaroids_zoom {\\r\\n\\theight: auto !important;\\r\\n\\tmax-width: 120px;\\r\\n}\\r\\n\\/* Other\\r\\n-------------------------------------------------------------------------*\\/\\r\\n\\/*Responsive media embed*\\/\\r\\nbody div.media_embed {\\r\\n\\tposition: relative; \\r\\n\\tpadding-bottom: 56.25%; \\r\\n\\tpadding-top: 30px; \\r\\n\\theight: 0; \\r\\n\\toverflow: hidden; \\r\\n\\tmax-width: 100%; \\r\\n\\theight: auto; \\r\\n\\tmargin-top:15px;\\r\\n} \\r\\nbody div.media_embed iframe, body div.media_embed object, body div.media_embed embed {\\r\\n\\tposition: absolute; top: 0; \\r\\n\\tleft: 0; \\r\\n\\twidth: 100%; \\r\\n\\theight: 100%; \\r\\n}\\r\\n\\r\\n\\/*Fixes*\\/\\r\\nbody.cke_ltr div.cke_panel_block div.scroll_box { height: 25px !important;} \\/* Minimises the height in the style preview list *\\/\\r\\nbody.cke_ltr div.cke_panel_block { color: #000000; } \\/* Stops templates setting the style list text to white *\\/","autolaunchFilebrowser":"1","minify":"1","magicline_enabled":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'System - JCK Modal', 'plugin', 'jckmodal', 'system', 0, 0, 1, 0, '{"name":"System - JCK Modal","type":"plugin","creationDate":"April 2011","author":"WebxSolution Ltd","copyright":"Copyright 2011 WebxSolution Ltd. All rights reserved.","authorEmail":"studio@webxsolution.com","authorUrl":"www.webxsolution.com","version":"1.1","description":"Forces Joomla to initialise the Modal JS classes required by JTree Link''s modal option.","group":"","filename":"jckmodal"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'com_jckman', 'component', 'com_jckman', '', 1, 1, 0, 0, '{"name":"com_jckman","type":"component","creationDate":"Jan 2015","author":"Andrew Williams","copyright":"2013 - 2015 WebxSolutions Ltd","authorEmail":"","authorUrl":"","version":"6.4.4","description":" \\n\\t<p>JoomlaCK Editor Manager v6.4<\\/p> \\n\\t","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'JCK Suite', 'package', 'pkg_jcksuite', '', 0, 1, 1, 0, '{"name":"JCK Suite","type":"package","creationDate":"Nov 2014","author":"Andrew Williams","copyright":"","authorEmail":"","authorUrl":"","version":"6.6.2","description":"PLG_JCK_SUITE_XML_DESC","group":"","filename":"pkg_jcksuite"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 'System - JCK Typography', 'plugin', 'jcktypography', 'system', 0, 1, 1, 0, 'false', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_filters`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `nobqg_finder_taxonomy`
--

INSERT INTO `nobqg_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_terms`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_finder_terms_common`
--

INSERT INTO `nobqg_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_finder_types`
--

CREATE TABLE IF NOT EXISTS `nobqg_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_jcklanguages`
--

CREATE TABLE IF NOT EXISTS `nobqg_jcklanguages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(5) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_jckplugins`
--

CREATE TABLE IF NOT EXISTS `nobqg_jckplugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL DEFAULT 'command',
  `row` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `editable` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `acl` text,
  `params` text NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Daten für Tabelle `nobqg_jckplugins`
--

INSERT INTO `nobqg_jckplugins` (`id`, `title`, `name`, `type`, `row`, `icon`, `published`, `editable`, `checked_out`, `checked_out_time`, `iscore`, `acl`, `params`, `parentid`) VALUES
(1, 'Scayt', 'scayt', 'plugin', 1, '-192', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 82),
(2, '', 'sourcearea', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(3, 'Source', 'source', 'command', 1, '0', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 2),
(4, 'Preview', 'preview', 'plugin', 1, '-64', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(5, 'Cut', 'cut', 'command', 1, '-96', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 60),
(6, 'Copy', 'copy', 'command', 1, '-112', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 60),
(7, 'Paste', 'paste', 'command', 1, '-128', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 60),
(8, 'PasteText', 'pastetext', 'plugin', 1, '-144', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(9, 'Find', 'find', 'plugin', 1, '-240', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(10, 'Replace', 'replace', 'command', 1, '-256', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 9),
(11, 'SelectAll', 'selectall', 'command', 1, '-272', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 61),
(12, 'RemoveFormat', 'removeformat', 'plugin', 1, '-288', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(13, 'Bold', 'bold', 'command', 2, '-304', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(14, 'Italic', 'italic', 'command', 2, '-320', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(15, 'Strike', 'strike', 'command', 2, '-352', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(16, 'Subscript', 'subscript', 'command', 2, '-368', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(17, 'Superscript', 'superscript', 'command', 2, '-384', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(18, 'Underline', 'underline', 'command', 2, '-336', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 58),
(19, 'Smiley', 'smiley', 'plugin', 2, '-640', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(20, 'Link', 'link', 'plugin', 2, '-528', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(21, 'Image', 'image', 'plugin', 2, '-576', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(22, 'Flash', 'flash', 'plugin', 2, '-592', 0, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(23, 'SpecialChar', 'specialchar', 'plugin', 2, '-656', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(24, 'PageBreak', 'pagebreak', 'plugin', 2, '-672', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(25, 'SpellChecker', 'checkspell', 'command', 1, '-192', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 82),
(26, '', 'tableresize', 'plugin', 2, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 82),
(27, '', 'tabletools', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 82),
(28, 'TextColor', 'textcolor', 'command', 3, '-704', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 62),
(29, 'BGColor', 'bgcolor', 'command', 3, '-720', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 62),
(30, 'Form', 'form', 'command', 1, '-752', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(31, 'Radio', 'radio', 'command', 1, '-784', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(32, 'TextField', 'textfield', 'command', 1, '-800', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(33, 'Textarea', 'textarea', 'command', 1, '-816', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(34, 'ShowBlocks', 'showblocks', 'plugin', 3, '-1136', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(35, 'Select', 'select', 'command', 1, '-832', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(36, 'ImageButton', 'imagebutton', 'command', 1, '-864', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(37, 'HiddenField', 'hiddenfield', 'command', 1, '-880', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(38, 'Checkbox', 'checkbox', 'command', 1, '-768', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(39, 'Button', 'button', 'command', 1, '-848', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 75),
(40, 'NumberedList', 'numberedlist', 'command', 2, '-400', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 90),
(41, 'BulletedList', 'bulletedlist', 'command', 2, '-416', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 90),
(42, 'Indent', 'indent', 'plugin', 2, '-448', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(43, 'Outdent', 'outdent', 'command', 2, '-432', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 42),
(44, 'JustifyLeft', 'justifyleft', 'command', 2, '-464', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 55),
(45, 'JustifyCenter', 'justifycenter', 'command', 2, '-480', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 55),
(46, 'JustifyBlock', 'justifyblock', 'command', 2, '-512', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 55),
(47, 'JustifyRight', 'justifyright', 'command', 2, '-496', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 55),
(48, 'Blockquote', 'blockquote', 'plugin', 2, '-1152', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(49, 'About', 'about', 'plugin', 3, '-736', 0, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(50, 'Maximize', 'maximize', 'plugin', 3, '-1040', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(51, '', 'div', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(52, 'CreateDiv', 'creatediv', 'command', 2, '-1168', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 51),
(53, '', 'editdiv', 'command', 0, '-1184', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 51),
(54, '', 'removediv', 'command', 0, '-1200', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 51),
(55, '', 'justify', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(56, '', 'a11yhelp', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(57, '', 'autogrow', 'plugin', 0, '', 0, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(58, '', 'basicstyles', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(59, 'Table', 'table', 'plugin', 2, '-608', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(60, '', 'clipboard', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(61, '', 'selection', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(62, '', 'colorbutton', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(63, 'Unlink', 'unlink', 'command', 2, '-544', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 20),
(64, 'Anchor', 'anchor', 'command', 2, '-560', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 20),
(65, '', 'contextmenu', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(66, '', 'editingblock', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(67, '', 'elementspath', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(68, '', 'enterkey', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(69, '', 'entities', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(70, '', 'toolbar', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(71, '', 'jfilebrowser', 'filebrowser', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(72, 'Styles', 'stylescombo', 'plugin', 3, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(73, 'Font', 'font', 'plugin', 3, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(74, 'Format', 'format', 'plugin', 3, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(75, '', 'forms', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(76, 'About', 'jabout', 'plugin', 3, '-736', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(77, 'Flash', 'jflash', 'plugin', 2, '-592', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(78, 'Save', 'jsave', 'plugin', 1, '-32', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(79, 'JTreeLink', 'jtreelink', 'plugin', 2, 'images/jtreelink.png', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(80, 'HorizontalRule', 'horizontalrule', 'plugin', 2, '-624', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(81, 'Print', 'print', 'plugin', 1, '-176', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(82, '', 'wsc', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(83, '', 'showborders', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(84, '', 'tab', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(85, 'Undo', 'undo', 'plugin', 1, '-208', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(86, 'Redo', 'redo', 'command', 1, '-224', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 86),
(87, '', 'resize', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(88, 'Templates', 'templates', 'plugin', 1, '-80', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(89, '', 'wysiwygarea', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(90, '', 'list', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(92, 'FontSize', 'fontsize', 'command', 3, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 73),
(93, 'PasteFromWord', 'pastefromword', 'plugin', 1, '-160', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(94, 'Mobileimage', 'mobileimage', 'plugin', 1, 'icon.png', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(95, '', 'html5support', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(96, 'Video', 'video', 'plugin', 3, 'images/icon.png', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 95),
(97, 'Audio', 'audio', 'plugin', 3, 'images/icon.png', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', 95),
(98, 'UIColor', 'uicolor', 'plugin', 3, 'uicolor.gif', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(99, '', 'imagedragndrop', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL),
(100, '', 'ie9selectionoverride', 'plugin', 0, '', 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_jcktoolbarplugins`
--

CREATE TABLE IF NOT EXISTS `nobqg_jcktoolbarplugins` (
  `toolbarid` int(11) NOT NULL,
  `pluginid` int(11) NOT NULL,
  `row` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`toolbarid`,`pluginid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_jcktoolbars`
--

CREATE TABLE IF NOT EXISTS `nobqg_jcktoolbars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `toolbar` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `nobqg_jcktoolbars`
--

INSERT INTO `nobqg_jcktoolbars` (`id`, `title`, `name`, `published`, `checked_out`, `checked_out_time`, `iscore`, `params`) VALUES
(1, 'Full', 'full', 1, 0, '0000-00-00 00:00:00', 1, ''),
(2, 'Publisher', 'publisher', 1, 0, '0000-00-00 00:00:00', 1, ''),
(3, 'Basic', 'basic', 1, 0, '0000-00-00 00:00:00', 1, ''),
(4, 'Blog', 'blog', 1, 0, '0000-00-00 00:00:00', 1, ''),
(5, 'Image', 'image', 1, 0, '0000-00-00 00:00:00', 1, ''),
(6, 'Mobile', 'mobile', 1, 0, '0000-00-00 00:00:00', 1, '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_languages`
--

CREATE TABLE IF NOT EXISTS `nobqg_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `nobqg_languages`
--

INSERT INTO `nobqg_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1),
(2, 'de-DE', 'German (DE-CH-AT)', 'Deutsch', 'de', 'de', '', '', '', '', 1, 0, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_menu`
--

CREATE TABLE IF NOT EXISTS `nobqg_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=128 ;

--
-- Daten für Tabelle `nobqg_menu`
--

INSERT INTO `nobqg_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 91, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 22, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 20, 21, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 23, 28, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 24, 25, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 26, 27, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 29, 30, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 31, 32, 0, '*', 1),
(18, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 33, 34, 0, '*', 1),
(19, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 35, 36, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 37, 38, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu-2', 'Startseite', 'startseite', '', 'startseite', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"3","num_columns":"3","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"0","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 41, 42, 1, '*', 0),
(102, 'main', 'COM_JCKMAN_MENU_NAME', 'com-jckman-menu-name', '', 'com-jckman-menu-name', 'index.php?option=com_jckman', 'component', 0, 1, 1, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/jcklogo.png', 0, '', 43, 58, 0, '', 1),
(103, 'main', 'COM_JCKMAN_SUBMENU_CPANEL_NAME', 'com-jckman-submenu-cpanel-name', '', 'com-jckman-menu-name/com-jckman-submenu-cpanel-name', 'index.php?option=com_jckman&view=cpanel', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-cpanel.png', 0, '', 44, 45, 0, '', 1),
(104, 'main', 'COM_JCKMAN_SUBMENU_PLUGIN_NAME', 'com-jckman-submenu-plugin-name', '', 'com-jckman-menu-name/com-jckman-submenu-plugin-name', 'index.php?option=com_jckman&view=list', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-pluginmanager.png', 0, '', 46, 47, 0, '', 1),
(105, 'main', 'COM_JCKMAN_SUBMENU_INSTALL_NAME', 'com-jckman-submenu-install-name', '', 'com-jckman-menu-name/com-jckman-submenu-install-name', 'index.php?option=com_jckman&view=install', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-installer.png', 0, '', 48, 49, 0, '', 1),
(106, 'main', 'COM_JCKMAN_SUBMENU_UNINSTALL_NAME', 'com-jckman-submenu-uninstall-name', '', 'com-jckman-menu-name/com-jckman-submenu-uninstall-name', 'index.php?option=com_jckman&view=extension', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-uninstaller.png', 0, '', 50, 51, 0, '', 1),
(107, 'main', 'COM_JCKMAN_SUBMENU_SYSTEMCHECK_NAME', 'com-jckman-submenu-systemcheck-name', '', 'com-jckman-menu-name/com-jckman-submenu-systemcheck-name', 'index.php?option=com_jckman&view=cpanel&taskbtn=system', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-systemcheck.png', 0, '', 52, 53, 0, '', 1),
(108, 'main', 'COM_JCKMAN_SUBMENU_SYNC_NAME', 'com-jckman-submenu-sync-name', '', 'com-jckman-menu-name/com-jckman-submenu-sync-name', 'index.php?option=com_jckman&view=cpanel&taskbtn=sync', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-sync.png', 0, '', 54, 55, 0, '', 1),
(109, 'main', 'COM_JCKMAN_SUBMENU_JCKEDITOR_NAME', 'com-jckman-submenu-jckeditor-name', '', 'com-jckman-menu-name/com-jckman-submenu-jckeditor-name', 'index.php?option=com_jckman&view=cpanel&taskbtn=editor', 'component', 0, 102, 2, 10003, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_jckman/icons/icon-16-editor.png', 0, '', 56, 57, 0, '', 1),
(110, 'mainmenu-2', 'Impressum & Datenschutz', 'impressum', '', 'impressum', 'index.php?option=com_content&view=article&id=2', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 59, 60, 0, '*', 0),
(112, 'mainmenu-2', 'Email schreiben', '2015-09-03-13-04-33', '', '2015-09-03-13-04-33', 'mailto:info@etisa.de', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 61, 62, 0, '*', 0),
(113, 'mainmenu-2', 'Anrufen', '2015-09-03-13-06-07', '', '2015-09-03-13-06-07', 'tel:015253397434', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 63, 64, 0, '*', 0),
(114, 'mainmenu-2', 'Jobs', 'jobs', '', 'jobs', 'index.php?option=com_content&view=article&id=4', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 65, 66, 0, '*', 0),
(115, 'mainmenu', 'Sprech- und Videoanlagen', 'sprechanlagen-und-videoanlagen', '', 'sprechanlagen-und-videoanlagen', 'index.php?option=com_content&view=article&id=18', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 67, 68, 0, '*', 0),
(116, 'mainmenu', 'Smarthome', 'smarthome', '', 'smarthome', 'index.php?option=com_content&view=article&id=18', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 69, 76, 0, '*', 0),
(117, 'mainmenu', 'Rolladensteuerung', 'rolladensteuerung', '', 'smarthome/rolladensteuerung', 'index.php?option=com_content&view=article&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 70, 71, 0, '*', 0),
(118, 'mainmenu', 'Alarmanlagen', 'alarmanlagen', '', 'smarthome/alarmanlagen', 'index.php?option=com_content&view=article&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 72, 73, 0, '*', 0),
(119, 'mainmenu', 'IP-Technik', 'ip-technik', '', 'smarthome/ip-technik', 'index.php?option=com_content&view=article&id=18', 'component', 1, 116, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 74, 75, 0, '*', 0),
(120, 'mainmenu', 'LED-Technik', 'led-technik', '', 'led-technik', 'index.php?option=com_content&view=article&id=11', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 77, 78, 0, '*', 0),
(121, 'mainmenu', 'Solarlösungen', 'solartechnik-und-photovoltaik', '', 'solartechnik-und-photovoltaik', 'index.php?option=com_content&view=article&id=18', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 79, 82, 0, '*', 0),
(122, 'mainmenu', 'Solarthermie', 'solarthermie', '', 'solartechnik-und-photovoltaik/solarthermie', 'index.php?option=com_content&view=article&id=18', 'component', 1, 121, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 80, 81, 0, '*', 0),
(123, 'mainmenu', 'Elektroinstallation', 'elektroinstallation', '', 'elektroinstallation', 'index.php?option=com_content&view=article&id=18', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 83, 86, 0, '*', 0),
(124, 'mainmenu', 'Elektroservice', 'elektroservice', '', 'elektroinstallation/elektroservice', 'index.php?option=com_content&view=article&id=18', 'component', 1, 123, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 84, 85, 0, '*', 0),
(126, 'mainmenu', 'SPS Steuerungen', 'sps-steuerungen', '', 'sps-steuerungen', 'index.php?option=com_content&view=article&id=18', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 87, 90, 0, '*', 0),
(127, 'mainmenu', 'Step7 Programmierung', 'step7-programmierung', '', 'sps-steuerungen/step7-programmierung', 'index.php?option=com_content&view=article&id=18', 'component', 1, 126, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 88, 89, 0, '*', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_menu_types`
--

CREATE TABLE IF NOT EXISTS `nobqg_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `nobqg_menu_types`
--

INSERT INTO `nobqg_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'mainmenu-2', 'Main Menu 2', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_messages`
--

CREATE TABLE IF NOT EXISTS `nobqg_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `nobqg_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_modules`
--

CREATE TABLE IF NOT EXISTS `nobqg_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=102 ;

--
-- Daten für Tabelle `nobqg_modules`
--

INSERT INTO `nobqg_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'mainnav1', 417, '2015-09-03 13:58:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"","showComponent":"1","separator":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 54, 'JCK Manager', '', '<img alt="" src="components/com_jckman/icons/jck-manager-logo.png" />', 1, 'jck_icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":0}', 1, ''),
(88, 55, 'Dashboard', '', '<img alt="" src="components/com_jckman/icons/jck-manager-logo.png" />', 2, 'jck_icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_jckquickicon', 1, 1, '', 1, ''),
(89, 56, 'JCK Manager v6.4.4', '', '<table class="table table-striped" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:0px;">\r\n			<tr>\r\n				<td>Version:</td>\r\n				<td>6.4.4</td>\r\n			</tr>\r\n			<tr>\r\n				<td>Author:</td>\r\n				<td><a href="http://www.joomlackeditor.com" target="_blank">www.joomlackeditor.com</a></td>\r\n			</tr>\r\n			<tr>\r\n				<td>Copyright:</td>\r\n				<td>&copy; WebxSolution Ltd, All rights reserved.</td>\r\n			</tr>\r\n			<tr>\r\n				<td>License:</td>\r\n				<td>GPLv2.0</td>\r\n			</tr>\r\n			<tr>\r\n				<td>More info:</td>\r\n				<td><a href="http://joomlackeditor.com/terms-of-use" target="_blank">http://joomlackeditor.com/terms-of-use</a></td>\r\n			</tr>\r\n		</table>', 3, 'jck_icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '', 1, ''),
(90, 57, 'COM_JCKMAN_CPANEL_SLIDER_MANAGER_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_MANAGER_HTML', 1, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(91, 58, 'COM_JCKMAN_CPANEL_SLIDER_PLUGIN_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_PLUGIN_HTML', 2, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(92, 59, 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_CHECK_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_CHECK_HTML', 3, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(93, 60, 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_LAYOUT_MANAGER', '', 'COM_JCKMAN_CPANEL_SLIDER_SYSTEM_LAYOUT_MANAGER_HTML', 4, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(94, 61, 'COM_JCKMAN_CPANEL_SLIDER_BACKUP_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_BACKUP_LABEL_HTML', 5, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(95, 62, 'COM_JCKMAN_CPANEL_SLIDER_RESTORE_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_RESTORE_HTML', 6, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(96, 63, 'COM_JCKMAN_CPANEL_SLIDER_SYNC_LABEL', '', 'COM_JCKMAN_CPANEL_SLIDER_SYNC_HTML', 7, 'jck_cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '', 1, 1, '{}', 1, ''),
(97, 65, 'Kontaktmodul', '', '<div class="title">\r\n	<h3>\r\n		Kontakt <b>aufnehmen</b></h3>\r\n	<p>\r\n		Für Sie haben wir immer ein offenes Ohr, eine Tasse Kaffee<br />\r\n		und den richtigen Ansprechparnter für Ihr Anliegen.</p>\r\n</div>\r\n<div class="email">\r\n	<i class="fa fa-envelope">&nbsp;</i>\r\n	<p>\r\n		Schreiben Sie uns eine E-Mail!</p>\r\n	<a href="mailto:info@etisa.de">info@etisa.de</a></div>\r\n<div class="telefon">\r\n	<i class="fa fa-mobile">&nbsp;</i>\r\n	<p>\r\n		Rufen Sie uns einfach an!</p>\r\n	<a href="tel:015253397434">0152 / 533 97 434</a></div>\r\n', 1, 'contact', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(98, 66, 'Philosophiemodul', '', '<h3>\r\n	Unsere <b>Philosophie</b></h3>\r\n<div class="box">\r\n	<i class="fa fa-line-chart">&nbsp;</i>\r\n	<p>\r\n		Entwicklung</p>\r\n</div>\r\n<div class="box">\r\n	<i class="fa fa-cube">&nbsp;</i>\r\n	<p>\r\n		Planung</p>\r\n</div>\r\n<div class="box">\r\n	<i class="fa fa-users">&nbsp;</i>\r\n	<p>\r\n		Beratung</p>\r\n</div>\r\n<div class="box">\r\n	<i class="fa fa-bullseye">&nbsp;</i>\r\n	<p>\r\n		Qualität</p>\r\n</div>\r\n', 1, 'philosophie', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(99, 67, 'Partnermodul', '', '<h3>\r\n	Unsere <b>Partner</b><i class="fa fa-angle-right">&nbsp;</i></h3>\r\n<div class="box">\r\n	<a href="http://www.wegnerhaus.de" target="_blank"> <img src="images/w-wegner-haus-gmbh-logo.png" /> </a></div>\r\n<div class="box">\r\n	<a href="http://www.wegnerhaus.de" target="_blank"> <img src="images/w-wegner-haus-gmbh-logo.png" /> </a></div>\r\n<div class="box">\r\n	<a href="http://www.wegnerhaus.de" target="_blank"> <img src="images/w-wegner-haus-gmbh-logo.png" /> </a></div>\r\n', 1, 'partner', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(100, 71, 'Main Menu 2', '', '', 0, 'mainnav2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu-2","base":"","startLevel":"1","endLevel":"0","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(101, 73, 'Themennavigationsmodul', '', '<div class="box item1">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition duo">\r\n			Sprech<b>anlagen</b><br />\r\n			Video<b>anlagen</b></h2>\r\n		<p class="vertical-align transition">\r\n			Bacon ipsum dolor amet chuck pork belly leberkas tri-tip. Sirloin rump biltong sausage picanha salami brisket, porchetta venison spare.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="sprechanlagen-und-videoanlagen.html">Mehr erfahren</a></div>\r\n<div class="box item2">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition">\r\n			Smart<b>home</b></h2>\r\n		<p class="vertical-align transition">\r\n			Ihr Haus kann dank Smarthome-Technologien, wie <a href="smarthome/rolladensteuerung.html">Rolladensteuerung</a>, <a href="smarthome/alarmanlagen.html">Alarmanlagen</a> und <a href="smarthome/ip-technik.html">IP-Technik</a> von überall aus gesteuert und betreut werden.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="smarthome.html">Mehr erfahren</a></div>\r\n<div class="box item3">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition">\r\n			Led <b>Technik</b></h2>\r\n		<p class="vertical-align transition">\r\n			Bacon ipsum dolor amet chuck pork belly leberkas tri-tip. Sirloin rump biltong sausage picanha salami brisket, porchetta venison spare.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="led-technik.html">Mehr erfahren</a></div>\r\n<div class="box item4">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition duo">\r\n			Solar<b>technik</b><br />\r\n			Photo<b>voltaik</b></h2>\r\n		<p class="vertical-align transition">\r\n			Bacon ipsum dolor amet chuck pork belly leberkas tri-tip. Sirloin rump biltong sausage picanha salami brisket, porchetta venison spare.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="solartechnik-und-photovoltaik.html">Mehr erfahren</a></div>\r\n<div class="box item5">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition duo">\r\n			Elektro<b>installation</b><br />\r\n			Elektro<b>service</b></h2>\r\n		<p class="vertical-align transition">\r\n			Bacon ipsum dolor amet chuck pork belly leberkas tri-tip. Sirloin rump biltong sausage picanha salami brisket, porchetta venison spare.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="elektroinstallation.html">Mehr erfahren</a></div>\r\n<div class="box item6">\r\n	<img src="images/sprechanlagen-videoanlagen.png" />\r\n	<div class="themeinfo">\r\n		<h2 class="vertical-align transition duo">\r\n			SPS <b>Steuerungen</b><br />\r\n			Step7 <b>Programmierung</b></h2>\r\n		<p class="vertical-align transition">\r\n			Bacon ipsum dolor amet chuck pork belly leberkas tri-tip. Sirloin rump biltong sausage picanha salami brisket, porchetta venison spare.</p>\r\n		<span class="transition button"><i class="fa fa-search">&nbsp;</i></span></div>\r\n	<a class="biglink" href="sps-steuerungen.html">Mehr erfahren</a></div>\r\n', 1, 'themennavigation', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_modules_menu`
--

CREATE TABLE IF NOT EXISTS `nobqg_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_modules_menu`
--

INSERT INTO `nobqg_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(97, 0),
(98, 101),
(99, 0),
(100, 0),
(101, 101);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `nobqg_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_overrider`
--

CREATE TABLE IF NOT EXISTS `nobqg_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_postinstall_messages`
--

CREATE TABLE IF NOT EXISTS `nobqg_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `nobqg_postinstall_messages`
--

INSERT INTO `nobqg_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_redirect_links`
--

CREATE TABLE IF NOT EXISTS `nobqg_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_schemas`
--

CREATE TABLE IF NOT EXISTS `nobqg_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_schemas`
--

INSERT INTO `nobqg_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.4.0-2015-02-26');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_session`
--

CREATE TABLE IF NOT EXISTS `nobqg_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_session`
--

INSERT INTO `nobqg_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('vvrl91p0rnj948dqefr4q63p36', 0, 0, '1441717055', '__default|a:8:{s:15:"session.counter";i:23;s:19:"session.timer.start";i:1441709197;s:18:"session.timer.last";i:1441717054;s:17:"session.timer.now";i:1441717055;s:22:"session.client.browser";s:73:"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0";s:8:"registry";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":1:{s:5:"users";O:8:"stdClass":1:{s:5:"login";O:8:"stdClass":1:{s:4:"form";O:8:"stdClass":2:{s:4:"data";a:0:{}s:6:"return";s:26:"http://localhost/etisa.de/";}}}}s:9:"separator";s:1:".";}s:4:"user";O:5:"JUser":28:{s:9:"\\0\\0\\0isRoot";b:1;s:2:"id";s:3:"417";s:4:"name";s:10:"Super User";s:8:"username";s:5:"admin";s:5:"email";s:28:"patrickfranz7@googlemail.com";s:8:"password";s:60:"$2y$10$PvBvKso8tjpZiB4Nel6rkulf/1vrhehDlZYjxYP78X.XPr3Y5xJMK";s:14:"password_clear";s:0:"";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:12:"registerDate";s:19:"2015-09-02 09:13:30";s:13:"lastvisitDate";s:19:"2015-09-08 10:17:58";s:10:"activation";s:1:"0";s:6:"params";s:0:"";s:6:"groups";a:1:{i:8;s:1:"8";}s:5:"guest";i:0;s:13:"lastResetTime";s:19:"0000-00-00 00:00:00";s:10:"resetCount";s:1:"0";s:12:"requireReset";s:1:"0";s:10:"\\0\\0\\0_params";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:14:"\\0\\0\\0_authGroups";a:2:{i:0;i:1;i:1;i:8;}s:14:"\\0\\0\\0_authLevels";a:5:{i:0;i:1;i:1;i:1;i:2;i:2;i:3;i:3;i:4;i:6;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:13:"\\0\\0\\0userHelper";O:18:"JUserWrapperHelper":0:{}s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;s:6:"otpKey";s:0:"";s:4:"otep";s:0:"";}s:13:"session.token";s:32:"9c539bbaf4b13cdc3b14d934acfe38c7";}', 417, 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_tags`
--

CREATE TABLE IF NOT EXISTS `nobqg_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `nobqg_tags`
--

INSERT INTO `nobqg_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 0, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_template_styles`
--

CREATE TABLE IF NOT EXISTS `nobqg_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `nobqg_template_styles`
--

INSERT INTO `nobqg_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.png","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}'),
(9, 'etisa', 0, '1', 'etisa - Standard', '{}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_ucm_base`
--

CREATE TABLE IF NOT EXISTS `nobqg_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_ucm_content`
--

CREATE TABLE IF NOT EXISTS `nobqg_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_ucm_history`
--

CREATE TABLE IF NOT EXISTS `nobqg_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Daten für Tabelle `nobqg_ucm_history`
--

INSERT INTO `nobqg_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 1, 1, '', '2015-09-03 11:49:38', 417, 2411, 'e3fcdb50202eaf2732a357cf13984b053ab3fe1a', '{"id":1,"asset_id":68,"title":"Herzlich Willkommen","alias":"herzlich-willkommen","introtext":"            <h1>Herzlich Willkommen bei <b>ETISA<\\/b><br \\/>\\r\\n                <strong>Ihrem Elektro Technik Industrie Service aus Paderborn<\\/strong>\\r\\n            <\\/h1>\\r\\n            <hr \\/>\\r\\n            <p>Landjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n            <img class=\\"senol\\" src=\\"images\\/etisa-geschaeftsfuehrer-senol-avci.png\\" \\/>","fulltext":"","state":1,"catid":"2","created":"2015-09-03 11:49:38","created_by":"417","created_by_alias":"","modified":"2015-09-03 11:49:38","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-03 11:49:38","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(2, 1, 1, '', '2015-09-03 11:50:50', 417, 2382, 'cd980b27543b1b745144f51857f29cd53a696078', '{"id":1,"asset_id":"68","title":"Herzlich Willkommen","alias":"herzlich-willkommen","introtext":"<h1>\\r\\n\\tHerzlich Willkommen bei <b>ETISA<\\/b><br \\/>\\r\\n\\t<strong>Ihrem Elektro Technik Industrie Service aus Paderborn<\\/strong><\\/h1>\\r\\n<hr \\/>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\t<img class=\\"senol\\" src=\\"images\\/etisa-geschaeftsfuehrer-senol-avci.png\\" \\/><\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-03 11:49:38","created_by":"417","created_by_alias":"","modified":"2015-09-03 11:50:50","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 11:49:38","publish_up":"2015-09-03 11:49:38","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"1","language":"*","xreference":""}', 0),
(3, 8, 5, '', '2015-09-03 12:00:01', 417, 549, '74eda23c2656e363a5c3a7abde0ce59a173e8fe6', '{"id":8,"asset_id":69,"parent_id":"1","lft":"11","rgt":12,"level":1,"path":null,"extension":"com_content","title":"Themen","alias":"themen","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"417","created_time":"2015-09-03 12:00:01","modified_user_id":null,"modified_time":"2015-09-03 12:00:01","hits":"0","language":"*","version":null}', 0),
(4, 9, 5, '', '2015-09-03 12:00:08', 417, 573, '9ea746a6bf849c600d0ca543584bb32f6a505a12', '{"id":9,"asset_id":70,"parent_id":"1","lft":"13","rgt":14,"level":1,"path":null,"extension":"com_content","title":"Andere Unterseiten","alias":"andere-unterseiten","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"417","created_time":"2015-09-03 12:00:08","modified_user_id":null,"modified_time":"2015-09-03 12:00:08","hits":"0","language":"*","version":null}', 0),
(9, 4, 1, '', '2015-09-03 13:06:47', 417, 1672, '99dce581d11a07bd600ecdcf0544f40d835fb095', '{"id":4,"asset_id":75,"title":"Jobs","alias":"jobs","introtext":"<h1>\\r\\n\\tJobs<\\/h1>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 13:06:47","created_by":"417","created_by_alias":"","modified":"2015-09-03 13:06:47","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-03 13:06:47","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(10, 5, 1, '', '2015-09-03 13:52:34', 417, 6981, 'cec438b9e1006917137528e84f5b751ec152c723', '{"id":5,"asset_id":76,"title":"Sprechanlagen & Videoanlagen","alias":"sprechanlagen-videoanlagen","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSprechanlagen &amp; Videoanlagen<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tHier steht die Unter-\\u00dcberschrift<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-03 13:52:34","created_by":"417","created_by_alias":"","modified":"2015-09-03 13:52:34","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-03 13:52:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(11, 5, 1, '', '2015-09-03 13:54:27', 417, 7000, '3005f87e4c19382b729528939bd90cb9e206c5fc', '{"id":5,"asset_id":"76","title":"Sprechanlagen & Videoanlagen","alias":"sprechanlagen-videoanlagen","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSprechanlagen &amp; Videoanlagen<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tHier steht die Unter-\\u00dcberschrift<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 13:52:34","created_by":"417","created_by_alias":"","modified":"2015-09-03 13:54:27","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 13:54:22","publish_up":"2015-09-03 13:52:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":4,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(12, 5, 1, '', '2015-09-03 15:36:41', 417, 7002, '5c53c9fe154ab7e8c834181295da8fd1ad23cdb1', '{"id":5,"asset_id":"76","title":"Sprechanlagen & Videoanlagen","alias":"sprechanlagen-videoanlagen","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSprechanlagen &amp; Videoanlagen<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tHier steht die Unter-\\u00dcberschrift<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 13:52:34","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:36:41","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 15:36:22","publish_up":"2015-09-03 13:52:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"1\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":5,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"27","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(13, 6, 1, '', '2015-09-03 15:40:19', 417, 1658, '20480df9070fff1fe1d01f3ed39addb3ffedd3ea', '{"id":6,"asset_id":77,"title":"Smarthome","alias":"smarthome","introtext":"","fulltext":"","state":1,"catid":"8","created":"2015-09-03 15:40:19","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:40:19","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-03 15:40:19","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(14, 5, 1, '', '2015-09-03 15:40:34', 417, 7002, '53a198690842f5c633b51b22291fd60dfb4ea4a1', '{"id":5,"asset_id":"76","title":"Sprechanlagen & Videoanlagen","alias":"sprechanlagen-videoanlagen","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSprechanlagen &amp; Videoanlagen<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tHier steht die Unter-\\u00dcberschrift<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 13:52:34","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:40:34","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 15:40:27","publish_up":"2015-09-03 13:52:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"1\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":7,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"29","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(15, 6, 1, '', '2015-09-03 15:41:11', 417, 6932, 'c57bf32d77e27f94655758c218a0f719167dfbc2', '{"id":6,"asset_id":"77","title":"Smarthome","alias":"smarthome","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSmarthome<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tLorem Ipsum Dolor sit ahmet<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 15:40:19","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:41:11","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 15:40:37","publish_up":"2015-09-03 15:40:19","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"1\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `nobqg_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(16, 6, 1, '', '2015-09-03 15:55:23', 417, 6931, '05788844849a571f1297001d254420614db47fbe', '{"id":6,"asset_id":"77","title":"Smarthome","alias":"smarthome","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSmarthome<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tLorem Ipsum Dolor sit ahmet<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 15:40:19","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:55:23","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 15:55:15","publish_up":"2015-09-03 15:40:19","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":4,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"3","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(17, 5, 1, '', '2015-09-03 15:55:32', 417, 7001, '713f92d80724bf8f2def7fadc3be07e3860c1afa', '{"id":5,"asset_id":"76","title":"Sprechanlagen & Videoanlagen","alias":"sprechanlagen-videoanlagen","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSprechanlagen &amp; Videoanlagen<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tHier steht die Unter-\\u00dcberschrift<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 13:52:34","created_by":"417","created_by_alias":"","modified":"2015-09-03 15:55:32","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-03 15:55:25","publish_up":"2015-09-03 13:52:34","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":8,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"34","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(18, 6, 1, '', '2015-09-03 16:00:09', 417, 6934, 'f4be0b73912b2ad08efd64edd86c71cfc0bbbe9a', '{"id":"6","asset_id":"77","title":"Smarthome","alias":"smarthome","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSmarthome<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tLorem Ipsum Dolor sit ahmet<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":"1","catid":"8","created":"2015-09-03 15:40:19","created_by":"417","created_by_alias":"","modified":"2015-09-03 16:00:09","modified_by":"417","checked_out":"0","checked_out_time":"0000-00-00 00:00:00","publish_up":"2015-09-03 15:40:19","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":"4","ordering":1,"metakey":"","metadesc":"","access":"1","hits":"10","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(19, 7, 1, '', '2015-09-07 09:42:23', 417, 1696, '83b63c68d020972807010ce058cb8e9ac8dc79b8', '{"id":7,"asset_id":78,"title":"Rolladensteuerung","alias":"rolladensteuerung","introtext":"<p>\\r\\n\\tPups<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 09:42:23","created_by":"417","created_by_alias":"","modified":"2015-09-07 09:42:23","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 09:42:23","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(20, 8, 1, '', '2015-09-07 10:01:32', 417, 1686, 'b6d4f21d48dde1edeba014eedeb4f541ff030f6f', '{"id":8,"asset_id":79,"title":"Alarmanlagen","alias":"alarmanlagen","introtext":"<p>\\r\\n\\tDups<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 10:01:32","created_by":"417","created_by_alias":"","modified":"2015-09-07 10:01:32","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 10:01:32","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(21, 9, 1, '', '2015-09-07 10:59:28', 417, 1680, '66028b2c8bc9e3e2b06bbf894f89e3853d34e980', '{"id":9,"asset_id":80,"title":"IP-Technik","alias":"ip-technik","introtext":"<p>\\r\\n\\tUl<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 10:59:28","created_by":"417","created_by_alias":"","modified":"2015-09-07 10:59:28","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 10:59:28","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(22, 10, 1, '', '2015-09-07 11:01:01', 417, 1689, '7bbe0604af883b4796f768f5f3fd438dbb735e3a', '{"id":10,"asset_id":81,"title":"Solarthermie","alias":"solarthermie","introtext":"<p>\\r\\n\\tshdfjk<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:01:01","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:01:01","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:01:01","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(23, 11, 1, '', '2015-09-07 11:04:00', 417, 1691, 'ce62d0fed702bb967c027b7008d26041f35ed08e', '{"id":11,"asset_id":82,"title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:04:00","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(24, 12, 1, '', '2015-09-07 11:05:01', 417, 1722, '4fca51e2c9dc3634c9a19b97f443229f1fe88f0a', '{"id":12,"asset_id":83,"title":"Solartechnik & Photovoltaik","alias":"solartechnik-photovoltaik","introtext":"<p>\\r\\n\\tdsfsdfsdfsd<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:05:01","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:05:01","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:05:01","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(25, 13, 1, '', '2015-09-07 11:08:50', 417, 1706, '97c7bd50ff514622fe3c8fd2d7f98854dbcb8047', '{"id":13,"asset_id":84,"title":"Elektroinstallation","alias":"elektroinstallation","introtext":"<p>\\r\\n\\tdfgdfgsdf<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:08:50","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:08:50","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:08:50","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(26, 14, 1, '', '2015-09-07 11:09:02', 417, 1700, 'a6ac3910d5c4ba5f902eb2570859a8aaa2613c30', '{"id":14,"asset_id":85,"title":"Elektroservice","alias":"elektroservice","introtext":"<p>\\r\\n\\tasdfasdfdasfs<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:09:02","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:09:02","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:09:02","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(28, 16, 1, '', '2015-09-07 11:09:32', 417, 1697, '5fdc41bc42f61077a3d77a97351a51730c2b8c4a', '{"id":16,"asset_id":87,"title":"SPS Steuerungen","alias":"sps-steuerungen","introtext":"<p>\\r\\n\\tfyd cfgg<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:09:32","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:09:32","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:09:32","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(29, 17, 1, '', '2015-09-07 11:09:47', 417, 1713, '3888544eca769076be5ee912c7dffac58726a430', '{"id":17,"asset_id":88,"title":"Step7 Programmierung","alias":"step7-programmierung","introtext":"<p>\\r\\n\\tgfagshdjfjkhjh<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:09:47","created_by":"417","created_by_alias":"","modified":"2015-09-07 11:09:47","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 11:09:47","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(31, 18, 1, '', '2015-09-07 15:08:27', 417, 1719, '7bdbf8c83148bb3f1e2ba92d765291f1789c7c9d', '{"id":18,"asset_id":89,"title":"Unsere Inhalte sind noch in Bearbeitung","alias":"unsere-inhalte-sind-noch-in-bearbeitung","introtext":"","fulltext":"","state":1,"catid":"2","created":"2015-09-07 15:08:27","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:08:27","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-09-07 15:08:27","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `nobqg_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(32, 6, 1, '', '2015-09-07 15:08:39', 417, 6933, '78fb0fd403c14112cc4749840108f0c2aa9c6261', '{"id":6,"asset_id":"77","title":"Smarthome","alias":"smarthome","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tSmarthome<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tLorem Ipsum Dolor sit ahmet<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<div class=\\"images clearfix\\">\\r\\n\\t<a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/1_b.jpg\\" title=\\"Lorem ipsum dolor sit amet\\"><img alt=\\"\\" src=\\"images\\/1_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/2_b.jpg\\" title=\\"Etiam quis mi eu elit temp\\"><img alt=\\"\\" src=\\"images\\/2_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/3_b.jpg\\" title=\\"Cras neque mi, semper leon\\"><img alt=\\"\\" src=\\"images\\/3_b.jpg\\" \\/><\\/a> <a class=\\"fancybox\\" data-fancybox-group=\\"gallery\\" href=\\"images\\/4_b.jpg\\" title=\\"Sed vel sapien vel sem uno\\"><img alt=\\"\\" src=\\"images\\/4_b.jpg\\" \\/><\\/a><\\/div>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n<h3>\\r\\n\\tPastrami leberkas capicola sausage<\\/h3>\\r\\n<p>\\r\\n\\tLandjaeger pork salami shank, beef turkey shankle meatball frankfurter drumstick. Pork loin flank ground round jerky, filet mignon salami ham hock sausage swine meatloaf hamburger andouille corned beef. Turducken beef ribs fatback ground round, spare ribs strip steak turkey. Chicken drumstick shankle fatback, turkey pork chop tri-tip rump sirloin kielbasa ribeye flank. Cow shankle andouille pork belly pork sausage.<\\/p>\\r\\n<p>\\r\\n\\tShort ribs spare ribs cow, meatball drumstick tongue pastrami salami chicken. Fatback pig strip steak, shankle flank biltong ham pancetta swine. Flank beef ribs shankle, rump pork loin bresaola porchetta picanha meatball boudin corned beef spare ribs alcatra leberkas. Hamburger rump doner shank capicola. Pork belly short loin pork loin, biltong frankfurter cupim tri-tip. Prosciutto bresaola turducken, beef ribs shankle kevin pork. Pancetta porchetta pork belly shankle venison salami swine drumstick shank. Pastrami brisket capicola, cow strip steak alcatra venison spare ribs short ribs prosciutto. Filet mignon hamburger biltong strip steak leberkas beef ribs pork prosciutto. Jowl corned beef bacon biltong landjaeger. Beef biltong meatloaf pork venison capicola alcatra bacon swine landjaeger kevin shankle jowl.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-03 15:40:19","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:08:39","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:08:33","publish_up":"2015-09-03 15:40:19","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":5,"ordering":"12","metakey":"","metadesc":"","access":"1","hits":"26","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(33, 18, 1, '', '2015-09-07 15:10:31', 417, 2056, '6ae10036ef5b08f54cd40dc8118899848f2f42eb', '{"id":18,"asset_id":"89","title":"Unsere Inhalte sind noch in Bearbeitung","alias":"unsere-inhalte-sind-noch-in-bearbeitung","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tWir bitte um Verst\\u00e4ndnis<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tUnsere Inhalte sind zur Zeit noch in Bearbeitung<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<p>\\r\\n\\tWir sind gerade dabei unsere Inhalte f\\u00fcr Sie zu optimieren, vielen Danke f\\u00fcr Ihr Verst\\u00e4ndnis. Ihr ETISA Team.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-07 15:08:27","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:10:31","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:08:42","publish_up":"2015-09-07 15:08:27","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(34, 18, 1, '', '2015-09-07 15:12:29', 417, 2057, '620dc4e5fa0365d3e9d3a4a661e40398c42811e1', '{"id":18,"asset_id":"89","title":"Unsere Inhalte sind noch in Bearbeitung","alias":"unsere-inhalte-sind-noch-in-bearbeitung","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tWir bitten um Verst\\u00e4ndnis<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tUnsere Inhalte sind zur Zeit noch in Bearbeitung<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<p>\\r\\n\\tWir sind gerade dabei unsere Inhalte f\\u00fcr Sie zu optimieren, vielen Danke f\\u00fcr Ihr Verst\\u00e4ndnis. Ihr ETISA Team.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-07 15:08:27","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:12:29","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:12:12","publish_up":"2015-09-07 15:08:27","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(35, 18, 1, '', '2015-09-07 15:13:42', 417, 2056, '0fff22751743b3843c586c061739d4481e7b15fd', '{"id":18,"asset_id":"89","title":"Unsere Inhalte sind noch in Bearbeitung","alias":"unsere-inhalte-sind-noch-in-bearbeitung","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tWir bitten um Verst\\u00e4ndnis<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tUnsere Inhalte sind zur Zeit noch in Bearbeitung<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<p>\\r\\n\\tWir sind gerade dabei unsere Inhalte f\\u00fcr Sie zu optimieren, vielen Dank f\\u00fcr Ihr Verst\\u00e4ndnis. Ihr ETISA Team.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-07 15:08:27","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:13:42","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:13:35","publish_up":"2015-09-07 15:08:27","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":4,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"1","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(36, 18, 1, '', '2015-09-07 15:18:31', 417, 2075, 'c96d6dfe877dc8ae5f64e63258245db7d0050b1d', '{"id":18,"asset_id":"89","title":"Unsere Inhalte sind noch in Bearbeitung","alias":"unsere-inhalte-sind-noch-in-bearbeitung","introtext":"<div class=\\"heading\\">\\r\\n\\t<h1>\\r\\n\\t\\tWir bitten um Verst\\u00e4ndnis<\\/h1>\\r\\n\\t<h2>\\r\\n\\t\\tUnsere Inhalte sind zur Zeit noch in Bearbeitung<\\/h2>\\r\\n\\t<hr \\/>\\r\\n<\\/div>\\r\\n<p>\\r\\n\\tWir sind gerade dabei unsere Inhalte f\\u00fcr Sie zu optimieren, vielen Dank f\\u00fcr Ihr Verst\\u00e4ndnis. Ihr <strong>ETISA<\\/strong> Team.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"2","created":"2015-09-07 15:08:27","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:18:31","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:18:23","publish_up":"2015-09-07 15:08:27","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":5,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"18","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(37, 2, 1, '', '2015-09-07 15:33:29', 417, 2582, 'bb45dde1a8a250f18ed78ac3fb28c67c32c85143', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\t<strong>Telefon:<\\/strong> 0 52 52 \\/ 525252<\\/p>\\r\\n<p>\\r\\n\\t<strong>Mobil:<\\/strong> 0152 \\/ 533 97 434<\\/p>\\r\\n<p>\\r\\n\\t<strong>Fax:<\\/strong> 0 52 52 \\/ 525252<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\t<strong>Mail:<\\/strong><a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<p>\\r\\n\\t<strong>Internet:<\\/strong> www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:33:29","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:31:54","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":6,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"18","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(38, 2, 1, '', '2015-09-07 15:34:45', 417, 2538, '5d195821d10a36b1bc0001cd18764cc6747c5169', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\t<strong>Telefon:<\\/strong> 0 52 52 \\/ 525252\\r\\n\\r\\n\\t<strong>Mobil:<\\/strong> 0152 \\/ 533 97 434\\r\\n\\r\\n\\t<strong>Fax:<\\/strong> 0 52 52 \\/ 525252<\\/p>\\r\\n\\r\\n<p>\\r\\n\\t<strong>Mail:<\\/strong><a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<p>\\r\\n\\t<strong>Internet:<\\/strong> www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:34:45","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:33:29","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":7,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"19","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(39, 2, 1, '', '2015-09-07 15:34:57', 417, 2550, 'fc6658e6f04145b500a6924b77c9458accddc4b1', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\t<strong>Telefon:<\\/strong> 0 52 52 \\/ 525252<\\/p>\\r\\n<p>\\r\\n\\t<strong>Mobil:<\\/strong> 0152 \\/ 533 97 434<\\/p>\\r\\n<p>\\r\\n\\t<strong>Fax:<\\/strong> 0 52 52 \\/ 525252<\\/p>\\r\\n<p>\\r\\n\\t<strong>Mail:<\\/strong><a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<p>\\r\\n\\t<strong>Internet:<\\/strong> www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:34:57","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:34:45","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":8,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"20","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(40, 2, 1, '', '2015-09-07 15:35:34', 417, 2547, 'bb8ce2154848bb0e490b196b341bc2549e78996e', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\t<strong>Telefon:<\\/strong> 0 52 52 \\/ 525252\\r\\n<br \\/>\\r\\n\\t<strong>Mobil:<\\/strong> 0152 \\/ 533 97 434\\r\\n<br \\/>\\r\\n\\t<strong>Fax:<\\/strong> 0 52 52 \\/ 525252\\r\\n<br \\/>\\r\\n\\t<strong>Mail:<\\/strong><a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<p>\\r\\n\\t<strong>Internet:<\\/strong> www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:35:34","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:34:57","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":9,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"21","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(41, 2, 1, '', '2015-09-07 15:36:15', 417, 2464, '8c19f0ac3a142d3e871652940adb0e05f5980a52', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<p>\\r\\n\\t<strong>Internet:<\\/strong> www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:36:15","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:35:34","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":10,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"22","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(42, 2, 1, '', '2015-09-07 15:36:49', 417, 2410, 'bb8c18e90510a555219a3c7d485a32df0b87faec', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<br \\/>\\r\\n\\tInternet: www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:36:49","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:36:15","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":11,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"23","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(43, 2, 1, '', '2015-09-07 15:37:36', 417, 2433, '6b6c97e5bfbe486b4cc0e71eae07c8887f925cb2', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra\\u00dfe 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<br \\/>\\r\\n<p>\\r\\n\\tInternet: www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen \\u00fcbernehmen wir keine Haftung f\\u00fcr die Inhalte anderer Internetpr\\u00e4senzen, die wir auf unserer Homepage verlinken. F\\u00fcr diese externen Inhalte sind ausschlie\\u00dflich deren Betreiber verantwortlich.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:37:36","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:36:49","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":12,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"24","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(44, 2, 1, '', '2015-09-07 15:47:55', 417, 4173, 'dbfae7e9a61605194a4280e30a537803e52166e3', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra&szlig;e 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<br \\/>\\r\\n<p>\\r\\n\\tInternet: www.etisa.de<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen &uuml;bernehmen wir keine Haftung f&uuml;r die Inhalte anderer Internetpr&auml;senzen, die wir auf unserer Homepage verlinken. F&uuml;r diese externen Inhalte sind ausschlie&szlig;lich deren Betreiber verantwortlich.<\\/p>\\r\\n\\r\\n<h3>Nutzung und Weitergabe personenbezogener Daten <\\/h3>\\r\\n<p>\\r\\n\\t\\r\\n\\tFalls Sie uns personenbezogene Daten ausgeh\\u00e4ndigt haben, (z.B \\u00fcber unser Kontaktformular) verwenden wir diese ausschlie\\u00dflich zur Erf\\u00fcllung Ihrer W\\u00fcnsche und Anforderungen oder zur Beantwortung Ihrer Kontaktformularanfrage. Die Weitergabe, der Verkauf oder sonstige \\u00dcbermittlungen Ihrer Daten an Dritte erfolgt nicht.<\\/p>\\r\\n\\r\\n\\tDiese Internetpr\\u00e4senz nutzt sogenannte Cookies, kleine Textinformationenstr\\u00e4ger, die der Browser bei Bedarf auf Ihrem System abspeichert, wenn Sie unsere Internetseiten besuchen. Zur Analyse unserer Internetseite nutzen wir Piwik, eine Webanalyse-Software, die die Daten der Cookies auf unserem System speichert. Diese Daten werden nicht an Dritte weitergegeben.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tEbenso verwendet Piwik \\u201eCookies\\u201c, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung unserer Website erm\\u00f6glichen. Die durch den Cookie erzeugten Informationen \\u00fcber Ihre Benutzung dieser Website werden im Piwik gespeichert. Ihre IP-Adresse wird dabei so ver\\u00e4ndert und vom System verschleiert, dass eine Zuordnung zu Ihnen nicht mehr m\\u00f6glich ist. Somit entsprechen wir also den Datenschutzbestimmungen.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tSollten Sie nicht w\\u00fcnschen, dass wir Ihren Computer wiedererkennen (Festplatten-Cookies), k\\u00f6nnen Sie Ihren Browser so einstellen, dass er Cookies von Ihrer Computerfestplatte l\\u00f6scht, alle Cookies blockiert oder Sie warnt, bevor ein Cookie gespeichert wird. Wir k\\u00f6nnen bei einer solchen Einstellung allerdings nicht zusichern, dass unsere Funktionen ohne Einschr\\u00e4nkung nutzenbar sind.<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:47:55","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:47:23","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":13,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"24","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(45, 2, 1, '', '2015-09-07 15:48:42', 417, 4185, '5ffccfe390424b4d38b6201ce1204fefcf44ff94', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra&szlig;e 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<br \\/>\\r\\n<p>\\r\\n\\tInternet: www.etisa.de<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen &uuml;bernehmen wir keine Haftung f&uuml;r die Inhalte anderer Internetpr&auml;senzen, die wir auf unserer Homepage verlinken. F&uuml;r diese externen Inhalte sind ausschlie&szlig;lich deren Betreiber verantwortlich.<\\/p>\\r\\n<h3>\\r\\n\\tNutzung und Weitergabe personenbezogener Daten<\\/h3>\\r\\n<p>\\r\\n\\tFalls Sie uns personenbezogene Daten ausgeh&auml;ndigt haben, (z.B &uuml;ber unser Kontaktformular) verwenden wir diese ausschlie&szlig;lich zur Erf&uuml;llung Ihrer W&uuml;nsche und Anforderungen oder zur Beantwortung Ihrer Kontaktformularanfrage. Die Weitergabe, der Verkauf oder sonstige &Uuml;bermittlungen Ihrer Daten an Dritte erfolgt nicht.<\\/p>\\r\\n<p>\\r\\n\\tDiese Internetpr&auml;senz nutzt sogenannte Cookies, kleine Textinformationenstr&auml;ger, die der Browser bei Bedarf auf Ihrem System abspeichert, wenn Sie unsere Internetseiten besuchen. Zur Analyse unserer Internetseite nutzen wir Piwik, eine Webanalyse-Software, die die Daten der Cookies auf unserem System speichert. Diese Daten werden nicht an Dritte weitergegeben.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tEbenso verwendet Piwik &bdquo;Cookies&ldquo;, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung unserer Website erm&ouml;glichen. Die durch den Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website werden im Piwik gespeichert. Ihre IP-Adresse wird dabei so ver&auml;ndert und vom System verschleiert, dass eine Zuordnung zu Ihnen nicht mehr m&ouml;glich ist. Somit entsprechen wir also den Datenschutzbestimmungen.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tSollten Sie nicht w&uuml;nschen, dass wir Ihren Computer wiedererkennen (Festplatten-Cookies), k&ouml;nnen Sie Ihren Browser so einstellen, dass er Cookies von Ihrer Computerfestplatte l&ouml;scht, alle Cookies blockiert oder Sie warnt, bevor ein Cookie gespeichert wird. Wir k&ouml;nnen bei einer solchen Einstellung allerdings nicht zusichern, dass unsere Funktionen ohne Einschr&auml;nkung nutzenbar sind.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:48:42","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:47:55","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":14,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"25","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `nobqg_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(46, 2, 1, '', '2015-09-07 15:48:57', 417, 4161, 'd74e277e4b324481c61c7bb2593f898338bd0fde', '{"id":2,"asset_id":"72","title":"Impressum & Datenschutz","alias":"impressum","introtext":"<h1>\\r\\n\\tImpressum &amp; Datenschutz<\\/h1>\\r\\n<p>\\r\\n\\tSenol Avci<br \\/>\\r\\n\\tStargarder Stra&szlig;e 2<br \\/>\\r\\n\\t33098 Paderborn<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tTelefon: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMobil: 0152 \\/ 533 97 434<br \\/>\\r\\n\\tFax: 0 52 52 \\/ 525252<br \\/>\\r\\n\\tMail:<a href=\\"mailto:info@etisa.de\\"> info@etisa.de<\\/a><\\/p>\\r\\n<br \\/>\\r\\n<p>\\r\\n\\tInternet: www.etisa.de<br \\/>\\r\\n\\tUSt.IdNr.: DE55555555555<\\/p>\\r\\n<p>\\r\\n\\tHaftungshinweis: Trotz inhaltlicher Kontrollen &uuml;bernehmen wir keine Haftung f&uuml;r die Inhalte anderer Internetpr&auml;senzen, die wir auf unserer Homepage verlinken. F&uuml;r diese externen Inhalte sind ausschlie&szlig;lich deren Betreiber verantwortlich.<\\/p>\\r\\n<h3>\\r\\n\\tNutzung und Weitergabe personenbezogener Daten<\\/h3>\\r\\n<p>\\r\\n\\tFalls Sie uns personenbezogene Daten ausgeh&auml;ndigt haben, (z.B &uuml;ber unser Kontaktformular) verwenden wir diese ausschlie&szlig;lich zur Erf&uuml;llung Ihrer W&uuml;nsche und Anforderungen oder zur Beantwortung Ihrer Kontaktformularanfrage. Die Weitergabe, der Verkauf oder sonstige &Uuml;bermittlungen Ihrer Daten an Dritte erfolgt nicht.<\\/p>\\r\\n<p>\\r\\n\\tDiese Internetpr&auml;senz nutzt sogenannte Cookies, kleine Textinformationenstr&auml;ger, die der Browser bei Bedarf auf Ihrem System abspeichert, wenn Sie unsere Internetseiten besuchen. Zur Analyse unserer Internetseite nutzen wir Piwik, eine Webanalyse-Software, die die Daten der Cookies auf unserem System speichert. Diese Daten werden nicht an Dritte weitergegeben.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tEbenso verwendet Piwik &bdquo;Cookies&ldquo;, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung unserer Website erm&ouml;glichen. Die durch den Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website werden im Piwik gespeichert. Ihre IP-Adresse wird dabei so ver&auml;ndert und vom System verschleiert, dass eine Zuordnung zu Ihnen nicht mehr m&ouml;glich ist. Somit entsprechen wir also den Datenschutzbestimmungen.<br \\/>\\r\\n\\t<br \\/>\\r\\n\\tSollten Sie nicht w&uuml;nschen, dass wir Ihren Computer wiedererkennen (Festplatten-Cookies), k&ouml;nnen Sie Ihren Browser so einstellen, dass er Cookies von Ihrer Computerfestplatte l&ouml;scht, alle Cookies blockiert oder Sie warnt, bevor ein Cookie gespeichert wird. Wir k&ouml;nnen bei einer solchen Einstellung allerdings nicht zusichern, dass unsere Funktionen ohne Einschr&auml;nkung nutzenbar sind.<\\/p>\\r\\n<p>\\r\\n\\t&nbsp;<\\/p>\\r\\n","fulltext":"","state":1,"catid":"9","created":"2015-09-03 12:01:54","created_by":"417","created_by_alias":"","modified":"2015-09-07 15:48:57","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-07 15:48:42","publish_up":"2015-09-03 12:01:54","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":15,"ordering":"2","metakey":"","metadesc":"","access":"1","hits":"26","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(47, 11, 1, '', '2015-09-08 08:31:16', 417, 1710, '47617f529d006d01a16d6e3f314bdf97a992ba29', '{"id":11,"asset_id":"82","title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-08 08:31:16","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-08 08:30:30","publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"6","metakey":"","metadesc":"","access":"1","hits":"1","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(48, 11, 1, '', '2015-09-08 08:32:15', 417, 1776, '940a4d21e5f6eec5601d34c81aa4ec7170ff5125', '{"id":11,"asset_id":"82","title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-08 08:32:15","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-08 08:31:37","publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":\\"\\\\\\/etisa.de\\\\\\/smarthome\\\\\\/ip-technik.html\\",\\"urlatext\\":\\"Zur\\u00fcck zum  Thema\\",\\"targeta\\":\\"0\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"6","metakey":"","metadesc":"","access":"1","hits":"1","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(49, 11, 1, '', '2015-09-08 08:41:46', 417, 1851, 'bacb660c1bfe9bb64835bd5ad6a4b4047f0baafd', '{"id":11,"asset_id":"82","title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-08 08:41:46","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-08 08:40:38","publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":\\"\\\\\\/etisa.de\\\\\\/smarthome\\\\\\/ip-technik.html\\",\\"urlatext\\":\\"Zur\\u00fcck zum Thema\\",\\"targeta\\":\\"0\\",\\"urlb\\":\\"\\\\\\/etisa.de\\\\\\/solartechnik-und-photovoltaik.html\\",\\"urlbtext\\":\\"Solartechnik & Photovoltaik\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":4,"ordering":"6","metakey":"","metadesc":"","access":"1","hits":"4","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(50, 11, 1, '', '2015-09-08 09:08:51', 417, 1842, '67d5cafd621ce7860f2314515a18eb5199979f1e', '{"id":11,"asset_id":"82","title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-08 09:08:51","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-08 09:08:28","publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":\\"\\\\\\/etisa.de\\\\\\/smarthome\\\\\\/ip-technik.html\\",\\"urlatext\\":\\"LED-Technik\\",\\"targeta\\":\\"0\\",\\"urlb\\":\\"\\\\\\/etisa.de\\\\\\/solartechnik-und-photovoltaik.html\\",\\"urlbtext\\":\\"Solartechnik & Photovoltaik\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":5,"ordering":"6","metakey":"","metadesc":"","access":"1","hits":"14","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(51, 11, 1, '', '2015-09-08 09:19:38', 417, 1841, 'ecc415792a3e2bc047a4118444e6004d3c092dc0', '{"id":11,"asset_id":"82","title":"LED-Technik","alias":"led-technik","introtext":"<p>\\r\\n\\tdfdsfsdfds<\\/p>\\r\\n","fulltext":"","state":1,"catid":"8","created":"2015-09-07 11:04:00","created_by":"417","created_by_alias":"","modified":"2015-09-08 09:19:38","modified_by":"417","checked_out":"417","checked_out_time":"2015-09-08 09:16:16","publish_up":"2015-09-07 11:04:00","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":\\"\\\\\\/etisa.de\\\\\\/smarthome\\\\\\/ip-technik.html\\",\\"urlatext\\":\\"IP-Technik\\",\\"targeta\\":\\"0\\",\\"urlb\\":\\"\\\\\\/etisa.de\\\\\\/solartechnik-und-photovoltaik.html\\",\\"urlbtext\\":\\"Solartechnik & Photovoltaik\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":8,"ordering":"6","metakey":"","metadesc":"","access":"1","hits":"20","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_updates`
--

CREATE TABLE IF NOT EXISTS `nobqg_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_update_sites`
--

CREATE TABLE IF NOT EXISTS `nobqg_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `nobqg_update_sites`
--

INSERT INTO `nobqg_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1441706328, ''),
(2, 'Joomla! Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1441706328, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 0, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'http://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 0, ''),
(5, 'JoomlaCK Editor Update Site', 'extension', 'http://www.joomlackeditor.com/upgrade/plg_jckeditor.xml', 1, 0, ''),
(6, 'JoomlaCK Editor Update Site', 'extension', 'http://www.joomlackeditor.com/upgrade/com_jckman.xml', 1, 0, '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `nobqg_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Daten für Tabelle `nobqg_update_sites_extensions`
--

INSERT INTO `nobqg_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600),
(3, 604),
(4, 28),
(5, 10001),
(6, 10003);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_usergroups`
--

CREATE TABLE IF NOT EXISTS `nobqg_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `nobqg_usergroups`
--

INSERT INTO `nobqg_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Öffentlich'),
(2, 1, 8, 15, 'Registriert'),
(3, 2, 9, 14, 'Autor'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Benutzer'),
(9, 1, 2, 3, 'Gast');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_users`
--

CREATE TABLE IF NOT EXISTS `nobqg_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=418 ;

--
-- Daten für Tabelle `nobqg_users`
--

INSERT INTO `nobqg_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(417, 'Super User', 'admin', 'patrickfranz7@googlemail.com', '$2y$10$PvBvKso8tjpZiB4Nel6rkulf/1vrhehDlZYjxYP78X.XPr3Y5xJMK', 0, 1, '2015-09-02 09:13:30', '2015-09-08 11:54:05', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_user_keys`
--

CREATE TABLE IF NOT EXISTS `nobqg_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_user_notes`
--

CREATE TABLE IF NOT EXISTS `nobqg_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_user_profiles`
--

CREATE TABLE IF NOT EXISTS `nobqg_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `nobqg_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `nobqg_user_usergroup_map`
--

INSERT INTO `nobqg_user_usergroup_map` (`user_id`, `group_id`) VALUES
(417, 8);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nobqg_viewlevels`
--

CREATE TABLE IF NOT EXISTS `nobqg_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `nobqg_viewlevels`
--

INSERT INTO `nobqg_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Öffentlich', 0, '[1]'),
(2, 'Registriert', 2, '[6,2,8]'),
(3, 'Spezial', 3, '[6,3,8]'),
(5, 'Gast', 1, '[9]'),
(6, 'Super Benutzer', 4, '[8]');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
