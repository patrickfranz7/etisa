<?php
defined( '_JEXEC' ) or die;
$this->setMetaData('generator','');
JHTML::_('behavior.framework', true);
$app          = JFactory::getApplication();
$sitename     = $app->getCfg('sitename');
$jinput = $app->input;
$view   = $jinput->get('view', '', 'string');
$oid    = $jinput->get('oid', '', 'int');
$option = $jinput->get('option', '', 'string');
$doc = JFactory::getDocument();
$this->_scripts = array();
unset($this->_script['text/javascript']);
$contentwidth = '';

if($this->countModules('themennavigation') == 0) $contentwidth = "main";
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <jdoc:include type="head" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

    <link rel="stylesheet" href="css/template.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/fancybox.css" type="text/css" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery.js"></script>
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/fancybox.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 13,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(51.7036466, 8.731411600000001), // paderborn

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(51.7036466, 8.731411600000001),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script>

</head>
<body>
    <div id="menu-layer">
        <div class="vertical-align clearfix">
            <div class="leftrow">
                <jdoc:include type="modules" name="mainnav1" style="none" />
            </div>
            <div class="rightrow">
                <jdoc:include type="modules" name="mainnav2" style="none" />
            </div>
        </div>
    </div>
    <header class="clearfix">
        <div class="center">
            <div class="logo">
                <a href="<?php echo $this->baseurl ?>">
                    <span class="logotext">ETISA</span>
                    <span class="logodesc">Elektro Technik Industrie Service Avci</span>
                </a>
            </div>
            <nav class="mainmenu clearfix">
                <a href="#" class="trigger">
                    <span class="menu">Menü</span>
                    <span class="bars">
                        <span class="bar_1 transition"></span>
                        <span class="bar_2 transition"></span>
                        <span class="bar_3 transition"></span>
                    </span>
                </a>
            </nav>
        </div>
    </header>

    <?php if ($this->countModules('themennavigation')) : ?>
    <section id="themen" class="clearfix">
        <jdoc:include type="modules" name="themennavigation" style="none" />
    </section>
    <?php endif; ?>

    <section id="content" class="<?php echo $contentwidth; ?>">

            <jdoc:include type="component" />

    </section>

    <?php if ($this->countModules('themennavigation')) : ?>
    <section id="philosophie">
        <div class="center clearfix">
            <jdoc:include type="modules" name="philosophie" style="none" />
        </div>
    </section>
    <?php endif; ?>

    <section id="partner">
        <div class="center clearfix">
            <jdoc:include type="modules" name="partner" style="none" />
        </div>
    </section>

    <section id="kontakt">
        <div class="center clearfix">
            <jdoc:include type="modules" name="contact" style="none" />
        </div>
    </section>

    <section id="map"></section>

    <footer>
        <div class="center">
            <span>
                <span>© 2015 <b>ETISA</b></span>
                <span class="seperator">|</span>
                    <a href="impressum.html">Impressum & Datenschutz</a>
            </span>
        </div>
    </footer>

    <script>
        jQuery(document).ready(function(){
            jQuery(".trigger").click(function() {
                jQuery("#menu-layer").slideToggle("fast");
                jQuery(".mainmenu").toggleClass("open");
                return false;
            });
        });

        jQuery(document).ready(function() {
            jQuery('.fancybox').fancybox();
        });
    </script>

</body>
</html>
