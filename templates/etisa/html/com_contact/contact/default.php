<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');
?>
<div class="contact">
  <h1><?php echo $this->contact->name; ?></h1>    
	<h2>Schreiben Sie uns eine Nachricht</h2>
	<?php if ($this->params->get('show_email_form') && ($this->contact->email_to || $this->contact->user_id)) : ?>
    <?php  echo $this->loadTemplate('form');  ?>
  <?php endif; ?>
</div>



