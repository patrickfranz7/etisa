<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

if (isset($this->error)) : ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php endif; ?>


<form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">  
      <p>Felder mit einem * sind Pflichtfelder</p>
      <input type="text" name="jform[contact_name]" id="jform_contact_name" value="" required="" placeholder="Ihr Name *" />
      <input type="email" name="jform[contact_email]" id="jform_contact_email" value="" required="" placeholder="Ihre Email *" />
      <input type="text" name="jform[contact_subject]" id="jform_contact_emailmsg" value="" required placeholder="Ihr Betreff *" />
      <textarea name="jform[contact_message]" id="jform_contact_message" required="" placeholder="Ihre Nachricht" ></textarea>
      

			<div class="control-group clearfix">
				<div class="controls left"><?php echo $this->form->getLabel('contact_email_copy'); ?></div>
				<div class="controls right"><?php echo $this->form->getInput('contact_email_copy'); ?></div>
			</div>
      

         
	    <button class="button" type="submit">Anfrage absenden</button>
      
			<input type="hidden" name="option" value="com_contact" />
			<input type="hidden" name="task" value="contact.submit" />
			<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
			<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		

</form>

